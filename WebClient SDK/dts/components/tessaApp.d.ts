import * as React from 'react';
import { Location } from 'history';
declare class TessaApp extends React.Component<TessaAppProps> {
    render(): JSX.Element;
}
export interface TessaAppProps {
    location: Location<any>;
}
export default TessaApp;
