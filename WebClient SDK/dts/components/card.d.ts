import * as React from 'react';
import { ICardEditorModel } from 'tessa/ui/cards';
declare class Card extends React.Component<CardProps> {
    render(): JSX.Element;
    private defaultRender;
    private specificRender;
}
export interface CardProps {
    cardEditor?: ICardEditorModel;
}
export default Card;
