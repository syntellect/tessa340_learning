import * as React from 'react';
declare class ViewSelectionDialog extends React.Component<IViewSelectionDialogProps, {}> {
    shouldComponentUpdate(nextProps: any): boolean;
    render(): JSX.Element;
}
export interface IViewSelectionDialogProps {
    noPortal?: boolean;
    isOpen?: boolean;
    values: any[];
    onClose: any;
    onSelection: any;
}
export default ViewSelectionDialog;
