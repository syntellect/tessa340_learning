import { Result } from 'tessa/platform';
import { IFileVersion } from 'tessa/files';
declare global {
    interface System {
        import(request: string): Promise<any>;
    }
    const System: System;
}
export interface ILazyPreviewerProps {
    previewFileVersion: IFileVersion | null;
    previewExtension: string;
    previewFileName: string;
    l: Function;
    previewPdfEnabled: boolean;
    tabMode: boolean;
    isPreviewTab: boolean;
    getFileContentFunc: (version: IFileVersion) => Promise<Result<File>>;
    localizeFunc: (value: string) => string;
    rotateAngles: {
        [key: number]: number;
    };
    setRotateAngle: (fileVersionId: string, angle: number) => void;
    pageNumber: number;
    setPageNumber: (value: number) => void;
}
export interface ILazyPreviewerState {
    component: any;
}
declare const _default: any;
export default _default;
