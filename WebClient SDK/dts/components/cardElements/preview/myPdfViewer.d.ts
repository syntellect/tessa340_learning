import React from 'react';
import { IDictionary } from 'common';
import { IFileVersion } from 'tessa/files';
import { Result } from 'tessa/platform';
import { ScaleOption } from 'tessa/ui/files/interfaces';
export default class MyPdfViewer extends React.Component<IMyPdfViewerProps, IMyPdfViewerState> {
    timeout: any;
    isMouseClick: boolean;
    lastClientX: any;
    lastClientY: any;
    containerPreviewRef: any;
    cardNode: any;
    mainDiv: HTMLDivElement | null;
    constructor(props: IMyPdfViewerProps);
    shouldComponentUpdate(nextProps: IMyPdfViewerProps, nextState: IMyPdfViewerState): boolean;
    componentWillReceiveProps(nextProps: IMyPdfViewerProps): void;
    componentDidUpdate(prevProps: IMyPdfViewerProps): void;
    componentDidMount(): void;
    componentWillUnmount(): void;
    onResize: () => void;
    onMouseDown: (e: any) => void;
    onMouseUp: () => void;
    onMouseMove: (e: any) => void;
    setWidth(forced?: boolean): void;
    getNewWidth(): any;
    getCardNode(): any;
    loadFile: (fileVersion: IFileVersion | null) => void;
    handleScaleChange: (e: any) => void;
    changeScale: (up: boolean) => void;
    onDocumentLoad: ({ numPages }: {
        numPages: any;
    }) => void;
    onDocumentError: (obj: any) => void;
    onPageLoad: ({ pageIndex, originalWidth }: {
        pageIndex: any;
        originalWidth: any;
    }) => void;
    changePage: (by: any) => void;
    getNextIndex: (index: number, total: number) => number;
    onInputPageBlur: (e: any) => void;
    onInputPageChange: (e: any) => void;
    onKeyDown: (event: any) => void;
    onWheel: (e: any) => void;
    setValue: (value: any) => void;
    setPageIndex: (pageIndex: any) => void;
    refHandler: (ref: any) => void;
    pageClickHandler: (page: number) => () => void;
    changeScaleHandler: (value: boolean) => () => void;
    handleRotateLeft: () => void;
    handleRotateRight: () => void;
    changeRotate: (value: number) => void;
    render(): JSX.Element;
}
export interface IMyPdfViewerProps {
    previewFileVersion: IFileVersion | null;
    getFileContentFunc: (version: IFileVersion) => Promise<Result<File>>;
    localizeFunc: (value: string) => string;
    locos: IDictionary<string>;
    tabMode: boolean;
    isPreviewTab: boolean;
    setRotateAngle: (fileVersionId: string, pageNumber: number, angle: number) => void;
    rotateAngles: {
        [key: number]: number;
    };
    pageNumber?: number;
    setPageNumber?: (value: number) => void;
    scale: ScaleOption;
    customScaleValue: number;
    setScale: (value: ScaleOption, customScaleValue: number) => void;
}
export interface IMyPdfViewerState {
    pageIndex: number;
    pageNumber: number;
    total: number;
    width: number;
    file?: any;
    error?: string;
    isLoading?: any;
    originalWidth: number;
    scaleValue: ScaleOption;
    customScaleValue: number;
    inputPageValue: string;
    rotateAngle: number;
    rotateAngles: {
        [key: number]: number;
    };
}
