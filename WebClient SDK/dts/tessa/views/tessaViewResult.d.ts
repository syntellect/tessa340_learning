import { DbType } from 'tessa/platform/dbType';
export interface ITessaViewResult {
    columns: string[];
    dataTypes: DbType[];
    schemeTypes: string[];
    result: Map<string, any>;
    rowCount: number;
    rows: any[];
    hasTimeOut: boolean;
    getColumnIndex(columnName: string): number;
}
export declare class TessaViewResult implements ITessaViewResult {
    constructor();
    readonly columnsKey = "columns";
    readonly dataTypesKey = "datatypes";
    readonly rowCountKey = "rowcount";
    readonly rowsKey = "rows";
    readonly schemeTypesKey = "schemetypes";
    columns: string[];
    dataTypes: DbType[];
    hasTimeOut: boolean;
    result: Map<string, any>;
    rowCount: number;
    rows: any[];
    schemeTypes: string[];
    getColumnIndex(columnName: string): number;
}
