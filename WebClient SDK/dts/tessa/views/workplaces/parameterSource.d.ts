import { WorkplaceCompositeMetadata } from './workplaceCompositeMetadata';
import { SourceKind } from './sourceKind';
import { IExtensionMetadata } from './extensionMetadata';
import { IWorkplaceComponentMetadata, WorkplaceMetadataComponentSealed } from './workplaceMetadataComponent';
import { IWorkplaceMetadataVisitor } from './workplaceMetadataVisitor';
import { IViewParameterMetadata, NodeClientVisibility } from '../metadata';
export interface IParameterSource extends IWorkplaceComponentMetadata {
    readonly sourceKind: SourceKind | null;
    seal<T = ParameterSourceSealed>(): T;
}
export interface ParameterSourceSealed extends WorkplaceMetadataComponentSealed {
    readonly sourceKind: SourceKind | null;
    seal<T = ParameterSourceSealed>(): T;
}
export declare class ParamSource extends WorkplaceCompositeMetadata implements IParameterSource {
    constructor();
    metadata: IViewParameterMetadata | null;
    readonly sourceKind: SourceKind | null;
    seal<T = ParameterSourceSealed>(): T;
}
export declare class MasterSource extends WorkplaceCompositeMetadata implements IParameterSource {
    constructor();
    metadata: IViewParameterMetadata | null;
    readonly sourceKind: SourceKind | null;
    seal<T = ParameterSourceSealed>(): T;
}
export declare class NullSource implements IParameterSource {
    constructor();
    _getType: string;
    alias: string;
    compositionId: guid;
    expandedIconName: string;
    extensions: Map<string, IExtensionMetadata>;
    iconName: string;
    orderPos: number;
    nodeClientVisibility: NodeClientVisibility;
    ownerId: guid;
    parentCompositionId: guid;
    readonly sourceKind: SourceKind | null;
    visit(_visitor: IWorkplaceMetadataVisitor): void;
    seal<T = ParameterSourceSealed>(): T;
}
