import { RequestCriteria } from './requestCriteria';
import { ViewParameterMetadataSealed } from './viewParameterMetadata';
export declare class RequestParameter {
    constructor();
    private _metadata;
    criteriaValues: RequestCriteria[];
    readonly isHidden: boolean;
    metadata: ViewParameterMetadataSealed | null;
    name: string | null;
    readOnly: boolean;
    clone(): RequestParameter;
    cloneAsReadOnly(): RequestParameter;
}
