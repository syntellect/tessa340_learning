import { SearchQueryMetadataSealed, ISearchQueryMetadata } from './searchQueries';
import { ValidationResult } from 'tessa/platform/validation';
export interface ISearchQueryService {
    allSearchQueries: ReadonlyArray<SearchQueryMetadataSealed>;
    getById(id: guid): SearchQueryMetadataSealed | null;
    initializeSearchQueries(searchQueries: ISearchQueryMetadata[] | SearchQueryMetadataSealed[]): any;
    save(metadata: ISearchQueryMetadata | SearchQueryMetadataSealed): Promise<ValidationResult | null>;
    delete(ids: guid[]): Promise<ValidationResult | null>;
}
export declare class SearchQueryService implements ISearchQueryService {
    private constructor();
    private static _instance;
    static readonly instance: ISearchQueryService;
    private _allSearchQueries;
    readonly allSearchQueries: SearchQueryMetadataSealed[];
    getById(id: guid): SearchQueryMetadataSealed | null;
    initializeSearchQueries(searchQueries: ISearchQueryMetadata[] | SearchQueryMetadataSealed[]): void;
    save(metadata: ISearchQueryMetadata | SearchQueryMetadataSealed): Promise<ValidationResult | null>;
    delete(ids: guid[]): Promise<ValidationResult | null>;
}
