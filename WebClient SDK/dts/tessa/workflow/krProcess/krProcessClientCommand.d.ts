import { StorageObject, IStorage } from 'tessa/platform/storage';
export declare class KrProcessClientCommand extends StorageObject {
    constructor(storage?: IStorage);
    static readonly commandTypeKey = "CommandType";
    static readonly parametersKey = "Parameters";
    readonly commandType: string;
    readonly parameters: IStorage;
}
