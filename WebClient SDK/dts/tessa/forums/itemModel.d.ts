import { AttachmentTypes } from './enums';
import { ValidationStorageObject } from 'tessa/platform/validation';
import { IStorage, IStorageValueFactory } from 'tessa/platform/storage';
export declare class ItemModel extends ValidationStorageObject {
    constructor(storage?: IStorage);
    static readonly messageIdKey: string;
    static readonly idKey: string;
    static readonly versionIdKey: string;
    static readonly uriKey: string;
    static readonly dataBase64Key: string;
    static readonly fileNameKey: string;
    static readonly captionKey: string;
    static readonly typeKey: string;
    messageId: guid | null;
    id: guid | null;
    versionId: guid | null;
    uri: string | null;
    dataBase64: string | null;
    fileName: string | null;
    caption: string | null;
    type: AttachmentTypes;
}
export declare class ItemModelFactory implements IStorageValueFactory<ItemModel> {
    getValue(storage: IStorage): ItemModel;
    getValueAndStorage(): {
        value: ItemModel;
        storage: IStorage;
    };
}
