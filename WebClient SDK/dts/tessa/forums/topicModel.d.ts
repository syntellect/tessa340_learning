import { MessageModel } from './messageModel';
import { ValidationStorageObject } from 'tessa/platform/validation';
import { IStorage, ArrayStorage, IStorageValueFactory } from 'tessa/platform/storage';
export declare class TopicModel extends ValidationStorageObject {
    constructor(storage?: IStorage);
    static readonly idKey: string;
    static readonly titleKey: string;
    static readonly descriptionKey: string;
    static readonly createdKey: string;
    static readonly isArchivedKey: string;
    static readonly lastUpdateDateTimeKey: string;
    static readonly authorIdKey: string;
    static readonly authorNameKey: string;
    static readonly lastMessageAuthorIdKey: string;
    static readonly lastMessageAuthorNameKey: string;
    static readonly lastReadMessageTimeKey: string;
    static readonly messagesKey: string;
    static readonly messagesCountKey: string;
    static readonly currentMessageNumberKey: string;
    static readonly currentParticipantInfoKey: string;
    id: guid;
    title: string | null;
    description: string | null;
    created: string | null;
    isArchived: boolean;
    lastUpdateDateTime: string | null;
    authorId: string | null;
    authorName: string | null;
    lastMessageAuthorId: string | null;
    lastMessageAuthorName: string | null;
    lastReadMessageTime: string | null;
    messages: ArrayStorage<MessageModel>;
    messagesCount: number;
    currentMessageNumber: number;
    currentParticipantInfo: IStorage;
    tryGetTopics(): ArrayStorage<MessageModel> | null | undefined;
    private static readonly _messageFactory;
    isEmpty(): boolean;
    clean(): void;
    ensureCacheResolved(): void;
}
export declare class TopicModelFactory implements IStorageValueFactory<TopicModel> {
    getValue(storage: IStorage): TopicModel;
    getValueAndStorage(): {
        value: TopicModel;
        storage: IStorage;
    };
}
