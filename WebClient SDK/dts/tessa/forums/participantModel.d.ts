import { ParticipantTypes } from './enums';
import { ValidationStorageObject } from 'tessa/platform/validation';
import { IStorage } from 'tessa/platform/storage';
export declare class ParticipantModel extends ValidationStorageObject {
    constructor(storage?: IStorage);
    static readonly topicIdKey: string;
    static readonly userIdKey: string;
    static readonly userNameKey: string;
    static readonly readOnlyKey: string;
    static readonly subscribedKey: string;
    static readonly typeIdKey: string;
    topicId: guid;
    userId: guid | null;
    userName: string | null;
    readOnly: boolean;
    subscribed: boolean;
    typeId: ParticipantTypes;
}
