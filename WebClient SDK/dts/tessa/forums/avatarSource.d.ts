import { ValidationStorageObject } from 'tessa/platform/validation';
import { IStorage } from 'tessa/platform/storage';
export declare class AvatarSource extends ValidationStorageObject {
    constructor(storage?: IStorage);
    static readonly cardIdKey: string;
    static readonly cardTypeIdKey: string;
    static readonly fileIdKey: string;
    static readonly versionRowIdKey: string;
    static readonly fileNameKey: string;
    cardId: string | null;
    cardTypeId: string | null;
    fileId: string | null;
    versionRowId: string | null;
    fileName: string | null;
}
