import { MessageBody } from './messageBody';
import { ValidationStorageObject } from 'tessa/platform/validation';
import { IStorage } from 'tessa/platform/storage';
export declare class MessageModelBase extends ValidationStorageObject {
    constructor(storage?: IStorage);
    static readonly idKey: string;
    static readonly bodyKey: string;
    static readonly authorIdKey: string;
    static readonly authorNameKey: string;
    static readonly topicIdKey: string;
    id: guid;
    body: MessageBody;
    authorId: string | null;
    authorName: string | null;
    topicId: guid;
    tryGetBody(): MessageBody | null | undefined;
    getMessageText(): string | null;
}
