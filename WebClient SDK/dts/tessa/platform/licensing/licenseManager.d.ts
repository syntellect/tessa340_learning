import { License } from './license';
import { IStorage } from '../storage';
export declare class LicenseManager {
    private constructor();
    private static _instance;
    static readonly instance: LicenseManager;
    private _license;
    readonly license: License;
    init(storage: IStorage): void;
}
