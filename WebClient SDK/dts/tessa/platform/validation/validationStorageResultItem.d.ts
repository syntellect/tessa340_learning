import { ValidationStorageObject } from './validationStorageObject';
import { ValidationResultType } from './validationResultType';
import { IValidationResultItem } from './validationResultItem';
import { IStorage, IStorageValueFactory } from 'tessa/platform/storage';
export declare class ValidationStorageResultItem extends ValidationStorageObject implements IValidationResultItem {
    constructor(storage?: IStorage);
    static readonly keyKey = "Key";
    static readonly typeKey = "Type";
    static readonly messageKey = "Message";
    static readonly fieldNameKey = "FieldName";
    static readonly objectNameKey = "ObjectName";
    static readonly objectTypeKey = "ObjectType";
    static readonly detailsKey = "Details";
    key: guid;
    type: ValidationResultType;
    message: string;
    fieldName: string;
    objectName: string;
    objectType: string;
    details: string;
    isEmpty(): boolean;
}
export declare class ValidationStorageResultItemFactory implements IStorageValueFactory<ValidationStorageResultItem> {
    getValue(storage: IStorage): ValidationStorageResultItem;
    getValueAndStorage(): {
        value: ValidationStorageResultItem;
        storage: IStorage;
    };
}
