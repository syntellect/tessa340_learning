import { IStorage } from 'tessa/platform/storage';
import { ICloneable } from 'tessa/platform';
import { ValidationInfoStorageObject, ValidationStorageResultBuilder } from 'tessa/platform/validation';
import { Dbms } from 'tessa/platform/dbms';
export declare class CardInitializationResponse extends ValidationInfoStorageObject implements ICloneable<CardInitializationResponse> {
    constructor(storage?: IStorage);
    static readonly dbmsKey: string;
    static readonly validationResultKey: string;
    static readonly configurationCacheIsActualKey: string;
    static readonly cardTypesKey: string;
    static readonly damagedCardTypeIdListKey: string;
    static readonly enumerationsKey: string;
    static readonly sectionsKey: string;
    dbms: Dbms;
    validationResult: ValidationStorageResultBuilder;
    configurationCacheIsActual: boolean;
    cardTypes: IStorage[];
    damagedCardTypeIdList: guid[];
    enumerations: IStorage[];
    sections: IStorage[];
    tryGetCardTypes(): IStorage[] | null | undefined;
    tryGetValidationResult(): ValidationStorageResultBuilder | null | undefined;
    tryGetDamagedCardTypeIdList(): guid[] | null | undefined;
    tryGetEnumerations(): IStorage[] | null | undefined;
    tryGetSections(): IStorage[] | null | undefined;
    clone(): CardInitializationResponse;
}
