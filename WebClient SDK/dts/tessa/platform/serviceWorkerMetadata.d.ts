import { IStorage } from 'tessa/platform/storage';
export declare class ServiceWorkerMetadata {
    static readonly serviceWorkerErrorKey: string;
    private storage;
    constructor(storage?: IStorage);
    readonly viewsCachedTime: string;
    readonly cardsCachedTime: string;
    serviceWorkerError: string;
}
