export declare enum FileSignatureState {
    NotChecked = 0,
    Checked = 1,
    Failed = 2
}
