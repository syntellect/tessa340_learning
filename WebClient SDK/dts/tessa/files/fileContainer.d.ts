import { IFileSource } from './fileSource';
import { IFile } from './file';
import { FileType, FileCategory, FileCreationToken, FileVersionCreationToken, IFileVersion, CertData } from 'tessa/files';
import { EventHandler, Result } from 'tessa/platform';
import { IUserSession } from 'common/utility/userSession';
import { IStorage } from 'tessa/platform/storage';
export interface FileContainerChangingEventArgs {
    added: IFile | null;
    removed: IFile | null;
    cancel: boolean;
    fileContiner: FileContainer;
}
export interface FileContainerChangedEventArgs {
    added: IFile | null;
    removed: IFile | null;
    fileContainer: FileContainer;
}
export interface IFileContainerPermissions {
    canAdd: boolean;
    seal<T = FileContainerPermissionsSealed>(): T;
}
export interface FileContainerPermissionsSealed {
    readonly canAdd: boolean;
    seal<T = FileContainerPermissionsSealed>(): T;
}
export declare class FileContainerPermissions implements IFileContainerPermissions {
    constructor();
    canAdd: boolean;
    seal<T = FileContainerPermissionsSealed>(): T;
}
export declare class FileContainer {
    constructor(source: IFileSource, permissions?: FileContainerPermissions | null);
    private readonly _source;
    private _files;
    private readonly _permissions;
    readonly source: IFileSource;
    readonly files: ReadonlyArray<IFile>;
    readonly permissions: FileContainerPermissionsSealed;
    init(files?: IFile[] | null): Promise<void>;
    dispose(): void;
    createFile(content: File, type: FileType, category?: FileCategory | null, name?: string | null, user?: IUserSession | null, modifyFileTokenAction?: ((token: FileCreationToken) => void) | null, modifyVersionTokenAction?: ((token: FileVersionCreationToken) => void) | null): IFile;
    addFile(file: IFile, isVirtual?: boolean, notify?: boolean): Promise<void>;
    removeFile(file: IFile, notify?: boolean): Promise<void>;
    getFileContent(file: IFile): Promise<Result<File>>;
    getFileContent(fileVersion: IFileVersion): Promise<Result<File>>;
    saveFileContent(file: IFile, info?: IStorage): Promise<Result<boolean>>;
    saveFileContent(fileVersion: IFileVersion, info?: IStorage): Promise<Result<boolean>>;
    getFileLink(file: IFile, info?: IStorage): string;
    getFileLink(fileVersion: IFileVersion, info?: IStorage): string;
    addSignature(version: IFileVersion, certificate: CertData, data: string, comment?: string, notify?: boolean): Promise<void>;
    getClonedFiles(source?: IFileSource | null): IFile[];
    readonly containerFileChanging: EventHandler<(args: FileContainerChangingEventArgs) => void>;
    readonly containerFileChanged: EventHandler<(args: FileContainerChangedEventArgs) => void>;
}
