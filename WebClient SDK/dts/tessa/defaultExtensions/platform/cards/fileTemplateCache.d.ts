import { FileTemplate } from './fileTemplate';
import { IStorage } from 'tessa/platform/storage';
export declare class FileTemplateCache {
    private constructor();
    private static _instance;
    static readonly instance: FileTemplateCache;
    private _templates;
    readonly templates: ReadonlyArray<FileTemplate>;
    getFromStorage(storage: IStorage): void;
}
