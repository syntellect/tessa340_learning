import { ApplicationExtension, IApplicationExtensionMetadataContext } from 'tessa';
export declare class WorkplacesExtension extends ApplicationExtension {
    afterMetadataReceived(context: IApplicationExtensionMetadataContext): void;
    private getFilteredWorkplacesMetadata;
    private getUserDefaultOpeningWorkplaces;
    private getWorkplaces;
}
