import { CardUIExtension, ICardUIExtensionContext } from 'tessa/ui/cards';
export declare class TopicsUIExtension extends CardUIExtension {
    private _context;
    private _forumViewModel;
    private _participants;
    private _participantReadOnly;
    private _participantModeratorMode;
    private _dispose;
    initialized(context: ICardUIExtensionContext): void;
    finalized(): void;
    private initForumControl;
    private disableCheckBox;
    private addParticipantsShowDialogWithoutTopicId;
    private saveTopics;
    private getParticipants;
    private getParticipantsList;
    private saveAddParticipants;
    private addTopicAction;
    private addParticipantsAction;
    private addRoleParticipantsAction;
    private openParticipantsAction;
    private checkAddTopicPermissionsAction;
    private checkSuperModeratorPermissionAction;
}
