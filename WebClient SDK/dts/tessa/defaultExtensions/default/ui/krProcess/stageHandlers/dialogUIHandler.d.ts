import { KrStageTypeUIHandler, IKrStageTypeUIHandlerContext, StageTypeHandlerDescriptor } from 'tessa/workflow/krProcess';
export declare class DialogUIHandler extends KrStageTypeUIHandler {
    private _cardStoreModeId;
    private _openModeId;
    private _dialogTypeId;
    descriptors(): StageTypeHandlerDescriptor[];
    validate(context: IKrStageTypeUIHandlerContext): void;
}
