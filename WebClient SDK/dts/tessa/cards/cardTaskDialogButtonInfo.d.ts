import { CardButtonType } from './cardButtonType';
import { IStorage, StorageObject, IStorageValueFactory } from 'tessa/platform/storage';
export declare class CardTaskDialogButtonInfo extends StorageObject {
    constructor(storage: IStorage);
    static readonly nameKey: string;
    static readonly cardButtonTypeKey: string;
    static readonly captionKey: string;
    static readonly iconKey: string;
    static readonly cancelKey: string;
    static readonly orderKey: string;
    name: string;
    cardButtonType: CardButtonType;
    caption: string;
    icon: string;
    cancel: boolean;
    order: number;
}
export declare class CardTaskDialogButtonInfoFactory implements IStorageValueFactory<CardTaskDialogButtonInfo> {
    getValue(storage: IStorage): CardTaskDialogButtonInfo;
    getValueAndStorage(): {
        value: CardTaskDialogButtonInfo;
        storage: IStorage;
    };
}
