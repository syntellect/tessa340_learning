import { WorkflowQueueSignal } from './workflowQueueSignal';
import { CardInfoStorageObject } from '../cardInfoStorageObject';
import { ICloneable } from 'tessa/platform';
import { IStorage, IStorageValueFactory } from 'tessa/platform/storage';
export declare class WorkflowQueueItem extends CardInfoStorageObject implements ICloneable<WorkflowQueueItem> {
    constructor(storage?: IStorage);
    static readonly idKey: string;
    static readonly handledKey: string;
    static readonly signalKey: string;
    static readonly queueEventTypeIdKey: string;
    static readonly queueEventTypeNameKey: string;
    id: guid;
    handled: boolean;
    queueEventTypeId: guid;
    queueEventTypeName: string;
    signal: WorkflowQueueSignal;
    ensureCacheResolved(): void;
    isEmpty(): boolean;
    clean(): void;
    tryGetSignal(): WorkflowQueueSignal | null | undefined;
    clone(): WorkflowQueueItem;
}
export declare class WorkflowQueueItemFactory implements IStorageValueFactory<WorkflowQueueItem> {
    getValue(storage: IStorage): WorkflowQueueItem;
    getValueAndStorage(): {
        value: WorkflowQueueItem;
        storage: IStorage;
    };
}
