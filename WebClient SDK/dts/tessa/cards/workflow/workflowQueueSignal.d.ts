import { CardInfoStorageObject } from '../cardInfoStorageObject';
import { ICloneable } from 'tessa/platform';
import { IStorage } from 'tessa/platform/storage';
export declare class WorkflowQueueSignal extends CardInfoStorageObject implements ICloneable<WorkflowQueueSignal> {
    constructor(storage?: IStorage);
    static readonly idKey: string;
    static readonly nameKey: string;
    static readonly numberKey: string;
    static readonly processIdKey: string;
    static readonly processTypeNameKey: string;
    static readonly typeIdKey: string;
    static readonly typeNameKey: string;
    static readonly parametersKey: string;
    id: guid;
    name: string;
    number: number;
    processId: guid | null;
    processTypeName: string | null;
    typeId: guid;
    typeName: string;
    parameters: IStorage;
    ensureCacheResolved(): void;
    tryGetParameters(): IStorage | null | undefined;
    isEmpty(): boolean;
    clone(): WorkflowQueueSignal;
}
