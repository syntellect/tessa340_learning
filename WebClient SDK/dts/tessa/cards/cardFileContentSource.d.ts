import { CardStorageObject } from './cardStorageObject';
import { IStorage } from 'tessa/platform/storage';
import { ICloneable } from 'tessa/platform';
export declare class CardFileContentSource extends CardStorageObject implements ICloneable<CardFileContentSource> {
    constructor(storage?: IStorage);
    static readonly cardIdKey = "CardID";
    static readonly cardTypeIdKey = "CardTypeID";
    static readonly fileIdKey = "FileID";
    static readonly versionRowIdKey = "VersionRowID";
    static readonly storeSourceKey = "StoreSource";
    static readonly originalVersionRowIdKey = "OriginalVersionRowID";
    cardId: guid;
    cardTypeId: guid;
    fileId: guid;
    versionRowId: guid;
    storeSource: number;
    originalVersionRowId: guid | null;
    clone(): CardFileContentSource;
}
