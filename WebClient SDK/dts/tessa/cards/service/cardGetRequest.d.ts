import { CardRequestBase } from './cardRequestBase';
import { IStorage, ArrayStorage } from 'tessa/platform/storage';
import { CardGetMode, CardGetTaskMode, CardNewMode, CardCompressionMode, CardGetMethod, CardGetRestrictionFlags } from 'tessa/cards';
import { ICloneable, TypedField, DotNetType } from 'tessa/platform';
export declare class CardGetRequest extends CardRequestBase implements ICloneable<CardGetRequest> {
    constructor(storage?: IStorage);
    static readonly getModeKey: string;
    static readonly getTaskModeKey: string;
    static readonly newModeKey: string;
    static readonly compressionModeKey: string;
    static readonly restrictionFlagsKey: string;
    static readonly sectionsToExcludeKey: string;
    static readonly authorTaskRowIdListKey: string;
    static readonly systemMethodKey: string;
    getMode: CardGetMode;
    getTaskMode: CardGetTaskMode;
    newMode: CardNewMode;
    compressionMode: CardCompressionMode;
    method: CardGetMethod;
    restrictionFlags: CardGetRestrictionFlags;
    sectionsToExclude: ArrayStorage<TypedField<DotNetType.String, string>>;
    authorTaskRowIdList: ArrayStorage<TypedField<DotNetType.Guid, guid>>;
    tryGetSectionsToExclude(): ArrayStorage<TypedField<DotNetType.String, string>> | null | undefined;
    tryGetAuthorTaskRowIdList(): ArrayStorage<TypedField<DotNetType.Guid, guid>> | null | undefined;
    ensureCacheResolved(): void;
    clone(): CardGetRequest;
}
