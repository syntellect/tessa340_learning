import { CardInfoStorageObject } from 'tessa/cards/cardInfoStorageObject';
import { IStorage, ArrayStorage } from 'tessa/platform/storage';
import { Card } from 'tessa/cards/card';
import { CardFile } from 'tessa/cards/cardFile';
export declare class CardCopyRequest extends CardInfoStorageObject {
    constructor(storage?: IStorage);
    static readonly exportedCardKey: string;
    static readonly sourceFileListKey: string;
    static readonly sourceCardIdKey: string;
    static readonly sourceCardTypeIdKey: string;
    static readonly sourceCardTypeNameKey: string;
    exportedCard: Card;
    sourceFileList: ArrayStorage<CardFile>;
    sourceCardId: guid;
    sourceCardTypeId: guid;
    sourceCardTypeName: string;
    private static readonly _fileFactory;
    tryGetExportedCard(): Card | null | undefined;
    tryGetSourceFileList(): ArrayStorage<CardFile> | null | undefined;
}
