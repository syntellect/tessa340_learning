import { CardNewMode, CardNewMethod } from 'tessa/cards';
import { CardInfoStorageObject } from 'tessa/cards/cardInfoStorageObject';
import { IStorage } from 'tessa/platform/storage';
import { ICloneable } from 'tessa/platform';
export declare class CardNewRequest extends CardInfoStorageObject implements ICloneable<CardNewRequest> {
    constructor(storage?: IStorage);
    static readonly cardTypeIdKey: string;
    static readonly cardTypeNameKey: string;
    static readonly newModeKey: string;
    static readonly systemMethodKey: string;
    cardTypeId: guid | null;
    cardTypeName: string | null;
    newMode: CardNewMode;
    method: CardNewMethod;
    clean(): void;
    clone(): CardNewRequest;
}
