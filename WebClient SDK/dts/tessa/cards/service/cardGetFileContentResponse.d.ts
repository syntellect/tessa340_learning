import { CardResponseBase } from './cardResponseBase';
import { IStorage } from 'tessa/platform/storage';
import { ICloneable } from 'tessa/platform';
export declare class CardGetFileContentResponse extends CardResponseBase implements ICloneable<CardGetFileContentResponse> {
    constructor(storage?: IStorage);
    static readonly fileNameKey: string;
    fileName: string | null;
    private _content;
    private _hasContent;
    private _size;
    readonly content: Blob | null;
    readonly hasContent: boolean;
    readonly size: number;
    setContent(content: Blob, fileName?: string | null): void;
    static tryGetFileNameFromContentDisposition(contentDisposition: string | null): string | null;
    clone(): CardGetFileContentResponse;
}
