import { CardResponseBase } from './cardResponseBase';
import { IStorage } from 'tessa/platform/storage';
export declare class CardStoreResponse extends CardResponseBase {
    constructor(storage?: IStorage);
    static readonly cardIdKey: string;
    static readonly cardTypeIdKey: string;
    static readonly cardVersionKey: string;
    static readonly storeDateTimeKey: string;
    cardId: guid;
    cardTypeId: guid;
    cardVersion: number;
    storeDateTime: string | null;
    clone(): CardStoreResponse;
}
