import { CardInfoStorageObject } from 'tessa/cards/cardInfoStorageObject';
import { IStorage, ArrayStorage } from 'tessa/platform/storage';
import { Card } from 'tessa/cards/card';
import { CardFile } from 'tessa/cards/cardFile';
export declare class CardCreateFromTemplateRequest extends CardInfoStorageObject {
    constructor(storage?: IStorage);
    static readonly templateCardKey: string;
    static readonly getExportedCardFromTemplateCardKey: string;
    static readonly exportedCardKey: string;
    static readonly sourceFileListKey: string;
    static readonly sourceCardIdKey: string;
    static readonly sourceCardTypeIdKey: string;
    static readonly sourceCardTypeNameKey: string;
    templateCard: Card;
    getExportedCardFromTemplateCard: boolean;
    exportedCard: Card;
    sourceFileList: ArrayStorage<CardFile>;
    sourceCardId: guid;
    sourceCardTypeId: guid;
    sourceCardTypeName: string;
    private static readonly _fileFactory;
    tryGetTemplateCard(): Card | null | undefined;
    tryGetExportedCard(): Card | null | undefined;
    tryGetSourceFileList(): ArrayStorage<CardFile> | null | undefined;
}
