import { CardInfoStorageObject } from 'tessa/cards/cardInfoStorageObject';
import { IStorage } from 'tessa/platform/storage';
export declare abstract class CardRequestBase extends CardInfoStorageObject {
    constructor(storage?: IStorage);
    static readonly cardIdKey: string;
    static readonly cardTypeIdKey: string;
    static readonly cardTypeNameKey: string;
    cardId: guid | null;
    cardTypeId: guid | null;
    cardTypeName: string | null;
    clean(): void;
}
