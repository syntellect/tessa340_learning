import { Card, CardStoreMethod } from 'tessa/cards';
import { CardInfoStorageObject } from 'tessa/cards/cardInfoStorageObject';
import { IStorage } from 'tessa/platform/storage';
import { ICloneable } from 'tessa/platform';
export declare class CardStoreRequest extends CardInfoStorageObject implements ICloneable<CardStoreRequest> {
    constructor(storage?: IStorage);
    static readonly cardKey: string;
    static readonly fileMappingKey: string;
    static readonly affectVersionKey: string;
    static readonly doesNotAffectVersionKey: string;
    static readonly forceTransactionKey: string;
    static readonly forceReleaseLockKey: string;
    static readonly systemMethodKey: string;
    card: Card;
    affectVersion: boolean;
    doesNotAffectVersion: boolean;
    forceTransaction: boolean;
    forceReleaseLock: boolean;
    method: CardStoreMethod;
    tryGetCard(): Card | null | undefined;
    clean(): void;
    ensureCacheResolved(): void;
    clone(): CardStoreRequest;
}
