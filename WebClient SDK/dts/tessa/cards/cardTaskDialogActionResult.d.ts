import { IStorage, StorageObject } from 'tessa/platform/storage';
import { ICloneable } from 'tessa/platform';
import { Card } from './card';
import { CardTaskDialogStoreMode } from './cardTaskDialogStoreMode';
export declare class CardTaskDialogActionResult extends StorageObject implements ICloneable<CardTaskDialogActionResult> {
    constructor(storage?: IStorage);
    static readonly mainCardIdKey: string;
    static readonly taskIdKey: string;
    static readonly pressedButtonNameKey: string;
    static readonly dialogCardIdKey: string;
    static readonly dialogCardKey: string;
    static readonly storeModeKey: string;
    mainCardId: guid;
    taskId: guid;
    pressedButtonName: string;
    dialogCardId: guid;
    dialogCard: Card | null;
    storeMode: CardTaskDialogStoreMode;
    setDialogCardId(cardId: guid): void;
    setDialogCard(card: Card): void;
    clone(): CardTaskDialogActionResult;
}
