import { CardStorageObject } from './cardStorageObject';
import { CardFileVersionState } from './cardFileVersionState';
import { IStorage, IStorageValueFactory } from 'tessa/platform/storage';
export declare class CardFileVersion extends CardStorageObject {
    constructor(storage?: IStorage);
    static readonly rowIdKey = "RowID";
    static readonly numberKey = "Number";
    static readonly nameKey = "Name";
    static readonly sizeKey = "Size";
    static readonly createdKey = "Created";
    static readonly createdByIdKey = "CreatedByID";
    static readonly createdByNameKey = "CreatedByName";
    static readonly hashKey = "Hash";
    static readonly sourceKey = "Source";
    static readonly stateKey = "State";
    static readonly errorDateKey = "ErrorDate";
    static readonly errorMessageKey = "ErrorMessage";
    static readonly optionsKey = "Options";
    static readonly linkIdKey = "LinkID";
    rowId: guid;
    number: number;
    name: string;
    size: number;
    created: string;
    createdById: guid;
    createdByName: string;
    linkId: guid | null;
    options: string | null;
    hash: string | null;
    source: number;
    state: CardFileVersionState;
    errorDate: string | null;
    errorMessage: string | null;
    isEmpty(): boolean;
    deserializeOptions(): any;
}
export declare class CardFileVersionFactory implements IStorageValueFactory<CardFileVersion> {
    getValue(storage: IStorage): CardFileVersion;
    getValueAndStorage(): {
        value: CardFileVersion;
        storage: IStorage;
    };
}
