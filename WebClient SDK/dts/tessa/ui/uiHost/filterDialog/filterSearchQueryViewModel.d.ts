import { SearchQueryMetadataSealed } from 'tessa/views/searchQueries';
export declare class FilterSearchQueryViewModel {
    constructor(viewAlias: string, currentQueryChanged: (query: SearchQueryMetadataSealed | null) => void);
    private _viewAlias;
    private _queries;
    private _publicQueries;
    private _userQueries;
    private _isPublicQueriesEnabled;
    private _isUserQueriesEnabled;
    private _isPublicQueriesOpen;
    private _isUserQueriesOpen;
    private _currentQuery;
    private _currentQueryChanged;
    isPublicQueriesEnabled: boolean;
    isUserQueriesEnabled: boolean;
    isPublicQueriesOpen: boolean;
    isUserQueriesOpen: boolean;
    readonly publicQueries: SearchQueryMetadataSealed[];
    readonly userQueries: SearchQueryMetadataSealed[];
    currentQuery: SearchQueryMetadataSealed | null;
    refresh(): void;
    private getPublicQueries;
    private getUserQueries;
}
