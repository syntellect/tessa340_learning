import * as React from 'react';
import { FilterSearchQueryViewModel } from './filterSearchQueryViewModel';
export interface FilterSearchQueryViewProps {
    viewModel: FilterSearchQueryViewModel;
}
export declare class FilterSearchQueryView extends React.Component<FilterSearchQueryViewProps> {
    private _mouseClicked;
    private _leftSideWidth;
    private _treeRef;
    componentDidMount(): void;
    componentWillUnmount(): void;
    render(): JSX.Element | null;
    private renderTree;
    private renderSearchQueries;
    private handleUserHeaderClick;
    private handlePublicHeaderClick;
    private handleCurrentQuery;
    private handleMouseUp;
    private handleMouseDown;
    private handleMouseMove;
}
