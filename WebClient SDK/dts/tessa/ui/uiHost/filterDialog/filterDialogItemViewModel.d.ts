import { FilterDialogCriteriaViewModel } from './filterDialogCriteriaViewModel';
import { RequestParameter } from 'tessa/views/metadata/requestParameter';
export declare class FilterDialogItemViewModel {
    constructor(parameter: RequestParameter);
    readonly parameter: RequestParameter;
    readonly values: FilterDialogCriteriaViewModel[];
    readonly caption: string;
    readonly readOnly: boolean;
    readonly isHidden: boolean;
    readonly isMultiply: boolean;
}
