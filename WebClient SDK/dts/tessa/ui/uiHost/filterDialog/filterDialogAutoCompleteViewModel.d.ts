import { IFilterDialogEditorViewModel } from './common';
import { ViewParameterMetadataSealed } from 'tessa/views/metadata';
import { CriteriaValue } from 'tessa/views/metadata/criteriaValue';
import { IAutoCompleteItem, IAutoCompletePopupItem } from 'tessa/ui/cards/controls/autoComplete';
import { SelectAction } from 'tessa/ui/views/selectFromViewContext';
export declare class FilterDialogAutoCompleteViewModel implements IFilterDialogEditorViewModel {
    constructor(meta: ViewParameterMetadataSealed);
    private _dataSource;
    private _popupItems;
    private _selectedItem;
    private _comboIsLoading;
    private _comboIsLoadingTimer;
    readonly meta: ViewParameterMetadataSealed;
    item: IAutoCompleteItem | null;
    readonly popupItems: ReadonlyArray<IAutoCompletePopupItem>;
    selectedItem: IAutoCompleteItem | null;
    readonly autoCompleteMode: boolean;
    readonly comboBoxMode: boolean;
    readonly hideSelectorButton: boolean;
    readonly popupDisplayIndexes: number[] | null;
    readonly comboIsLoading: boolean;
    selectAction: SelectAction | null;
    deleteAction: () => void | null;
    readonly maxResultsCount: number;
    changeItem(text: string, value: string): void;
    getValue(): CriteriaValue;
    setItem(item: IAutoCompletePopupItem | null): void;
    setItemFromViews(): Promise<void>;
    deleteItem(): void;
    findItems(filter: string | null): Promise<ReadonlyArray<IAutoCompletePopupItem>>;
    getDefaultCaption(popupItem: IAutoCompletePopupItem): string;
}
