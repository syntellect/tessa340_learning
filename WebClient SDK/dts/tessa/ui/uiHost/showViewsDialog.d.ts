import { SelectAction } from '../views';
import { SelectedValue } from 'tessa/views';
import { RequestParameter } from 'tessa/views/metadata/requestParameter';
export declare function showViewsDialog(refSection: string, parameters?: RequestParameter[] | null, selectAction?: SelectAction | null): AsyncIterableIterator<SelectedValue | null>;
export declare function showSelectionDialog(selectedValues: SelectedValue[]): Promise<SelectedValue | null>;
