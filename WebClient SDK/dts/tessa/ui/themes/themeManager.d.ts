import { Theme } from './theme';
import { IStorage } from 'tessa/platform/storage';
export declare class ThemeManager {
    private constructor();
    private static _instance;
    static readonly instance: ThemeManager;
    private _themes;
    private _isInitialized;
    readonly themes: Map<string, Theme>;
    readonly isInitialized: boolean;
    readonly currentTheme: Theme;
    readonly wallpapersNameMap: Map<string, string>;
    initialize(themes: IStorage): void;
    tryGetTheme(name: string): Theme | null;
}
