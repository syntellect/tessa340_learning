import { ICardEditorModel } from './cards/interfaces';
import { IViewContext } from './views/viewContext';
import { IStorage } from 'tessa/platform/storage';
import { ScopeContextInstance } from 'tessa/platform/scopes';
import { ITileWorkspace } from 'tessa/ui/tiles/interfaces';
import { CreateCardArg, OpenCardArg, ShowCardArg } from './uiHost/common';
export interface IUIContext {
    readonly cardEditor: ICardEditorModel | null;
    readonly viewContext: IViewContext | null;
    tiles: ITileWorkspace | null;
    readonly info: IStorage;
    readonly parent: IUIContext | null;
    actionOverridings: IUIContextActionOverridings | null;
}
export interface IUIContextActionOverridings {
    createCard?: (args: CreateCardArg) => Promise<ICardEditorModel | null>;
    openCard?: (args: OpenCardArg) => Promise<ICardEditorModel | null>;
    showCardEditor?: (args: ShowCardArg) => Promise<ICardEditorModel | null>;
}
export declare class UIContext implements IUIContext {
    constructor(args: {
        cardEditor?: ICardEditorModel;
        viewContext?: IViewContext | (() => IViewContext);
        info?: IStorage;
        actionOverridings?: IUIContextActionOverridings;
    });
    private static _scopeContext;
    private _cardEditor;
    private _viewContext;
    private _viewContextFunc;
    private _info;
    private _parent;
    readonly cardEditor: ICardEditorModel | null;
    readonly viewContext: IViewContext | null;
    tiles: ITileWorkspace | null;
    readonly info: IStorage;
    readonly parent: IUIContext | null;
    static readonly current: IUIContext;
    static readonly hasCurrent: boolean;
    static readonly unknown: IUIContext;
    actionOverridings: IUIContextActionOverridings | null;
    static create(context: IUIContext | null): ScopeContextInstance<IUIContext>;
}
