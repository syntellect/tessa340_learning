import { BaseContentItem } from './baseContentItem';
import { ContentPlaceArea } from './contentPlaceArea';
import { IWorkplaceViewComponent } from '../workplaceViewComponent';
import { Visibility } from 'tessa/platform';
export declare class MultiSelectButtonViewModel extends BaseContentItem {
    constructor(viewComponent: IWorkplaceViewComponent, area?: ContentPlaceArea, order?: number);
    private _visibility;
    readonly isLoading: boolean;
    readonly multiSelectEnabled: boolean;
    visibility: Visibility;
    changeMultiSelectEnabled(): void;
}
