import { UserStatusModel } from 'tessa/forums';
export declare class ForumClientCachedDataManager {
    private constructor();
    private static _instance;
    static readonly instance: ForumClientCachedDataManager;
    private _readTopicIdList;
    readonly readTopicIdList: Map<guid, UserStatusModel>;
    addReadedTopicId(topicId: guid, lastReadMessageTime: string): void;
    addTopicId(topicId: guid): void;
}
