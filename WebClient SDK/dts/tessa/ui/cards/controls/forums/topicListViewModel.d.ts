import { ForumViewModel } from './forumViewModel';
import { TopicViewModel } from './topicViewModel';
import { IAttachmentMenuContext } from './forumCommon';
import { ForumItemViewModel } from './forumItemViewModel';
import { UIButton } from 'tessa/ui/uiButton';
import { MenuAction } from 'tessa/ui/menuAction';
export declare class TopicListViewModel {
    constructor(forumViewModel: ForumViewModel, models: TopicViewModel[]);
    readonly forumViewModel: ForumViewModel;
    private readonly _topics;
    private _leftButtons;
    private _rightButtons;
    readonly topics: TopicViewModel[];
    readonly leftButtons: UIButton[];
    readonly rightButtons: UIButton[];
    readonly attachmentContextMenuGenerators: ((ctx: IAttachmentMenuContext) => void)[];
    private initDefaultButtons;
    private initAttachmentContextMenu;
    getAttachmentContextMenu(item: ForumItemViewModel): MenuAction[];
    dispose(): void;
}
