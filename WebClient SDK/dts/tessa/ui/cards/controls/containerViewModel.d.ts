import { ControlViewModelBase } from './controlViewModelBase';
import { CardTypeTabControl, CardTypeControl } from 'tessa/cards/types';
import { ICardModel, IFormViewModel } from '../interfaces';
import { ValidationResultBuilder } from 'tessa/platform/validation';
export declare class ContainerViewModel extends ControlViewModelBase {
    constructor(control: CardTypeTabControl, parentControl: CardTypeControl | null, model: ICardModel);
    private _form;
    form: IFormViewModel;
    readonly isEmpty: boolean;
    onUnloading(validationResult: ValidationResultBuilder): void;
}
