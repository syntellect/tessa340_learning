/// <reference types="react" />
import { ControlViewModelBase } from './controlViewModelBase';
import { ICardModel } from '../interfaces';
import { CardTypeControl } from 'tessa/cards/types';
import { ValidationResultBuilder } from 'tessa/platform/validation';
/**
 * Модель представления для вывода кнопки.
 */
export declare class ButtonViewModel extends ControlViewModelBase {
    constructor(control: CardTypeControl, model: ICardModel);
    private _cardModel;
    private _text;
    /**
     * Текст кнопки.
     */
    text: string;
    /**
     * Команда, выполняемая при нажатии на кнопку.
     */
    onClick: ((e: React.SyntheticEvent<HTMLButtonElement>) => void) | null;
    executeOnClick(e: React.SyntheticEvent<HTMLButtonElement>): void;
    onUnloading(validationResult: ValidationResultBuilder): void;
}
