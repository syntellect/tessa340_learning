/// <reference types="react" />
import { GridColumnInfo } from './gridColumnInfo';
import { Card, CardRow } from 'tessa/cards';
export declare class GridCellViewModel {
    constructor(row: CardRow, card: Card, columnInfo: GridColumnInfo);
    private _row;
    private _card;
    private _columnInfo;
    private readonly internalValue;
    readonly value: any;
    readonly toolTip: string;
    readonly columnInfo: GridColumnInfo;
    readonly style: React.CSSProperties;
    private getStyle;
}
