import { FileSignatureState, IFileVersion } from 'tessa/files';
export declare class FileListSignsDialogViewModel {
    constructor(version: IFileVersion);
    readonly version: IFileVersion;
    readonly signState: FileSignatureState;
    readonly signStateText: string;
}
