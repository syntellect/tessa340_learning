import { FileFiltering } from './fileFiltering';
import { FileViewModel } from '../fileViewModel';
export declare class FileInternalFiltering extends FileFiltering {
    constructor(func: (file: FileViewModel) => boolean);
    private _func;
    isVisible(viewModel: FileViewModel): boolean;
    equals(other: FileFiltering): boolean;
}
