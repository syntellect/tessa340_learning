import { CertData } from 'tessa/files';
export declare class FileListSelectCertDialogViewModel {
    constructor(certs: CertData[]);
    readonly certs: ReadonlyArray<CertData>;
    selectedCertIndex: number;
    readonly selectedCert: CertData | null;
    setSelectedCert(index: number): void;
}
