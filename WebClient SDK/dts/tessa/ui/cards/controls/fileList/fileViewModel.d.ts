import { FileTagViewModel } from './fileTagViewModel';
import { IFile } from 'tessa/files';
export declare class FileViewModel {
    constructor(file: IFile);
    private _caption;
    private _isLoading;
    private _tag;
    private _toolTip;
    readonly model: IFile;
    readonly id: guid;
    caption: string;
    canDownload: boolean;
    isLoading: boolean;
    readonly isModified: boolean;
    tag: FileTagViewModel | null;
    toolTip: string;
    private getDefaultToolTip;
}
