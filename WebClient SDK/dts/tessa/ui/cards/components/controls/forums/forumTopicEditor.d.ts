import * as React from 'react';
import { TopicEditorViewModel } from 'tessa/ui/cards/controls';
export interface ForumTopicEditorProps {
    viewModel: TopicEditorViewModel;
}
export declare class ForumTopicEditor extends React.Component<ForumTopicEditorProps> {
    private _bodyRef;
    private _editorInputRef;
    componentDidMount(): void;
    componentDidUpdate(prevProps: ForumTopicEditorProps): void;
    render(): JSX.Element;
    private renderMessages;
    private renderSeparator;
    private handleSendButtonClick;
    private scrollBodyToEnd;
}
