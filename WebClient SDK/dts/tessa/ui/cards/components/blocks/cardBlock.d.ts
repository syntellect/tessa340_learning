import * as React from 'react';
import { IBlockViewModel } from 'tessa/ui/cards/interfaces';
export interface CardBlockProps {
    viewModel: IBlockViewModel;
}
export declare class CardBlock extends React.Component<CardBlockProps> {
    private _blockComponent;
    componentWillMount(): void;
    componentWillUpdate(nextProps: CardBlockProps): void;
    render(): JSX.Element;
}
