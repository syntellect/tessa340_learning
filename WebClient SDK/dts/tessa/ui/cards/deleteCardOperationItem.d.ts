import { CardOperationItem } from './cardOperationItem';
import { CardMetadataSealed } from 'tessa/cards/metadata';
import { CardDeletionMode } from 'tessa/cards';
export declare class DeleteCardOperationItem<T = guid> extends CardOperationItem<T> {
    constructor(cardId: T, displayValue: string);
    private _withoutBackupOnly;
    readonly withoutBackupOnly: boolean;
    private _deletionMode;
    readonly deletionMode: CardDeletionMode;
    setupType(typeId: guid | null, withoutBackupOnly: boolean, cardMetadata: CardMetadataSealed): void;
}
