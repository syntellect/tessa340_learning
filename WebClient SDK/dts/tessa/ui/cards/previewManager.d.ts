import { IFile, IFileVersion } from 'tessa/files';
import { Result, EventHandler } from 'tessa/platform';
import { IFileControlManager, ScaleOption } from '../files';
export declare class PreviewManager implements IFileControlManager {
    constructor();
    private atom;
    protected _filesAngles: {
        [key: string]: {
            [key: number]: number;
        };
    };
    private _fileVersion;
    private _fileName;
    private _fileExtension;
    private _pageNumber;
    private _scale;
    private _customScaleValue;
    private _previewPdfEnabled;
    pageChanged: EventHandler<(args: {
        fileVersionId: string;
        page: number;
    }) => void>;
    rotateChanged: EventHandler<(args: {
        fileVersionId: string;
        page: number;
        angle: number;
    }) => void>;
    scaleChanged: EventHandler<(args: {
        fileVersionId: string;
        scale: ScaleOption;
        customScaleValue: number;
    }) => void>;
    enabled: boolean;
    readonly fileVersion: IFileVersion | null;
    readonly fileName: string | null;
    readonly fileExtension: string | null;
    readonly previewPdfEnabled: boolean;
    readonly scale: ScaleOption;
    readonly pageNumber: number;
    readonly customScaleValue: number;
    setPageNumber(value: number): void;
    setEnabled(enabled?: boolean): void;
    showPreview(file: IFile): void;
    reset(): void;
    resetIfInPreview(versionId: guid): void;
    handleFileContentLoading: (version: IFileVersion) => Promise<Result<File>>;
    /**
     * Углы поворота файлов в предпросмотре
     *
     * @readonly
     * @type {{ [key: string]: number }}
     * @memberof PreviewFormViewModel
     */
    readonly filesAngles: {
        [key: string]: {
            [key: string]: number;
        };
    };
    readonly fileAngles: {
        [key: string]: number;
    };
    setRotateAngle(fileVersionId: string, page: number, angle: number): void;
    setScale(scale: ScaleOption, customScaleValue?: number): void;
}
