import { ICardToolbarViewModel } from './cardToolbarViewModel';
import { ITileContext } from '../tiles';
export interface CardToolbarItemCommand {
    (item: CardToolbarItem): void | Promise<void>;
}
export declare abstract class CardToolbarItem {
    constructor(args: {
        name: string;
        caption: string;
        icon: string;
        toolTip?: string;
        order?: number;
    });
    protected _toolbar: ICardToolbarViewModel;
    private _caption;
    private _icon;
    private _toolTip;
    private _order;
    private _hotkey;
    private _actualTooltip;
    private _command;
    private _backgroundColor;
    private _foregroundColor;
    private _backgroundHoverColor;
    private _foregroundHoverColor;
    readonly name: string;
    caption: string;
    icon: string;
    toolTip: string;
    order: number;
    hotkey: string;
    readonly command: CardToolbarItemCommand | null;
    backgroundColor: string | null;
    foregroundColor: string | null;
    foregroundHoverColor: string | null;
    backgroundHoverColor: string | null;
    attach(toolbar: ICardToolbarViewModel): void;
    setCommand(command: CardToolbarItemCommand): void;
    executeCommand(tileContext: ITileContext): Promise<void>;
    rebuildTooltip(fakeHotKey?: string | null): void;
}
