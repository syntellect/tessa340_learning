import { IFormViewModel, IBlockViewModel, IControlViewModel } from '../interfaces';
import { SupportUnloadingViewModel } from '../supportUnloadingViewModel';
import { CardTypeBlock, CardTypeBlockSealed } from 'tessa/cards/types';
import { Visibility } from 'tessa/platform/visibility';
import { IValidationResultBuilder } from 'tessa/platform/validation';
export declare abstract class BlockViewModelBase extends SupportUnloadingViewModel implements IBlockViewModel {
    constructor(block: CardTypeBlock, caption: string, captionVisibility: Visibility, leftCaptions: boolean, collapsed: boolean, doNotCollapseWithTopBlock: boolean, columnIndex?: number, rowIndex?: number, columnSpan?: number, rowSpan?: number);
    protected _form: IFormViewModel;
    protected _caption: string;
    protected _captionVisibility: Visibility;
    protected _blockVisibility: Visibility;
    protected _blockMargin: string | null;
    protected _controls: ReadonlyArray<IControlViewModel>;
    protected _controlMargin: string | null;
    protected _leftCaptions: boolean;
    protected _verticalInterval: number;
    protected _horizontalInterval: number;
    protected _columnIndex?: number;
    protected _rowIndex?: number;
    protected _columnSpan?: number;
    protected _rowSpan?: number;
    protected _collapsed: boolean;
    /**
     * Нужно ли схлопывать вместе с верхним блоком
     */
    readonly doNotCollapseWithTopBlock: boolean;
    readonly componentId: guid;
    /**
     * Информация о типе отображаемого блока.
     */
    readonly cardTypeBlock: CardTypeBlockSealed;
    /**
     * Имя блока, по которому он доступен в коллекции.
     */
    readonly name: string | null;
    /**
     * Форма, в которой размещён текущий блок.
     */
    readonly form: IFormViewModel;
    /**
     * Заголовок блока.
     */
    caption: string;
    /**
     * Видимость заголовка блока.
     */
    captionVisibility: Visibility;
    /**
     * Видимость блока.
     */
    blockVisibility: Visibility;
    /**
     * Предпочитаемый отступ блока относительно других блоков.
     */
    blockMargin: string | null;
    /**
     * Упорядоченная коллекция элементов управления в блоке, доступная только для чтения.
     */
    readonly controls: ReadonlyArray<IControlViewModel>;
    /**
     * Предпочитаемый отступ элемента управления относительно других элементов управления.
     */
    controlMargin: string | null;
    /**
     * Заголовок блока.
     */
    leftCaptions: boolean;
    /**
     * Вертикальный интервал
     */
    verticalInterval: number;
    /**
     * Горизонтальный интервал
     */
    horizontalInterval: number;
    /**
     * Индекс колонки
     *
     * @readonly
     * @type {(number | undefined)}
     * @memberof BlockViewModelBase
     */
    columnIndex: number | undefined;
    /**
     * Индекс строки
     */
    rowIndex: number | undefined;
    /**
     * Кол-во занимаемых колонок
     */
    columnSpan: number | undefined;
    /**
     * Кол-во занимаемых строк
     */
    rowSpan: number | undefined;
    /**
     * Свернут ли блок.
     */
    collapsed: boolean;
    /**
     * Признак того, что блок не содержит отображаемых данных.
     */
    readonly isEmpty: boolean;
    setForm(form: IFormViewModel): void;
    getCollapsibleBottomBlocks(): IBlockViewModel[];
    private setCollapsedForBottomBlocks;
    onUnloading(validationResult: IValidationResultBuilder): void;
}
