import { InteractionEventTypes, ElementSide } from 'common/utility';
export declare class SidePanelManager {
    private constructor();
    private static _instance;
    static readonly instance: SidePanelManager;
    private _openedPanel;
    private _interactionType;
    readonly openedPanel: ElementSide | null;
    readonly isLeftPanelOpened: boolean;
    readonly isRigthPanelOpened: boolean;
    readonly interactionType: InteractionEventTypes | null;
    openPanel(side: ElementSide, type: InteractionEventTypes): void;
    closePanel(): void;
}
