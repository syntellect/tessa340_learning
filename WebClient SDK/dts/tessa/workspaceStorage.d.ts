import { TabPanelButton } from './ui/tabPanelButton';
import { CardWorkspace } from 'tessa/ui/cardWorkspace';
import { ViewWorkspace } from 'tessa/ui/viewWorkspace';
import { WorkspaceModel } from 'tessa/ui/workspaceModel';
import { WorkplaceMetadataSealed } from 'tessa/views/workplaces';
export interface AvailableViewWorkspaceInfo {
    id: guid;
    caption: string;
    metadata: WorkplaceMetadataSealed;
}
export declare class WorkspaceStorage {
    private constructor();
    private static _instance;
    static readonly instance: WorkspaceStorage;
    static createContextInstance(): () => void;
    private _cards;
    private _views;
    private _orderedStorage;
    private _currentId;
    private _lastDeactivatedId;
    readonly uiId: guid;
    neverDispatch: boolean;
    readonly currentWorkspace: WorkspaceModel | null;
    readonly cards: ReadonlyMap<guid, CardWorkspace>;
    readonly currentCardWorkspace: CardWorkspace | null;
    readonly views: ReadonlyMap<guid, ViewWorkspace>;
    readonly currentViewWorkspace: ViewWorkspace | null;
    readonly orderedStorage: ReadonlyArray<WorkspaceModel>;
    readonly availableViewWorkplace: AvailableViewWorkspaceInfo[];
    readonly tabPanelButtons: TabPanelButton[];
    private setCurrentWorkspaceId;
    activateWorkspace(id: guid, needDispatch?: boolean): any;
    activateWorkspace(workspace: WorkspaceModel, needDispatch?: boolean): any;
    tryGetWorkspace(id: guid): WorkspaceModel | null;
    addWorkspace(workspace: WorkspaceModel, order?: number): void;
    removeWorkspace(workspace: WorkspaceModel): number;
    tryGetCard(id: guid): CardWorkspace | null;
    tryGetCardByCardId(cardId: guid): CardWorkspace | null;
    tryGetView(id: guid): ViewWorkspace | null;
    getWorkspaceOrder(id: guid): number;
    getWorkspaceOrder(workspace: WorkspaceModel): number;
    changeWorkspaceOrder(id: guid, order: number): any;
    changeWorkspaceOrder(workspace: WorkspaceModel, order: number): any;
    changeWorkspaceOrderByTarget(sourceWorkplace: WorkspaceModel, targetWorkplace: WorkspaceModel): void;
    private rebuildCache;
    private onWorkspaceClosed;
    private activateAnotherPossibleWorkspace;
    scrollWorkplaceIntoView(workspace: WorkspaceModel): void;
}
