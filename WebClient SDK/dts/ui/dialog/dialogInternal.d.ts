import * as React from 'react';
declare class DialogInternal extends React.Component<IDialogInternalProps, {}> {
    layerIndex: number;
    leaveTimer: any;
    enterTimer: any;
    overlayClick: boolean;
    static defaultProps: {
        isAutoSize: boolean;
        isOverlayHidden: boolean;
        closeByEsc: boolean;
        okByEnter: boolean;
        autofocus: boolean;
    };
    windowRef: React.RefObject<HTMLDivElement>;
    layoutRef: React.RefObject<HTMLDivElement>;
    componentWillMount(): void;
    componentDidMount(): void;
    componentWillUnmount(): void;
    componentWillAppear(callback: any): void;
    componentWillEnter(callback: any): void;
    componentDidAppear(): void;
    componentDidEnter(): void;
    componentWillLeave(callback: any): void;
    initializeAnimation(callback: any): void;
    animate(value: any): void;
    handleOverlayClick: (event: any) => void;
    handleKeyDown: (event: React.KeyboardEvent<Element>) => void;
    render(): JSX.Element;
}
export interface IDialogInternalProps {
    children?: any;
    className?: string;
    style?: object;
    isAutoSize?: boolean;
    isOverlayHidden?: boolean;
    onCloseRequest?: any;
    onClick?: any;
    closeByEsc?: boolean;
    okByEnter?: boolean;
    onKeyDown?: any;
    autofocus?: boolean;
    [key: string]: any;
}
export default DialogInternal;
