import React from 'react';
import { IDictionary } from 'common';
declare class Dialog extends React.Component<DialogProps, DialogState> {
    private _el;
    constructor(props: DialogProps);
    componentDidMount(): void;
    componentWillUnmount(): void;
    firstChild(props: DialogProps): any;
    render(): JSX.Element;
    internalRender(): JSX.Element;
}
export interface DialogState {
    mounted: boolean;
}
export interface DialogProps {
    children?: any;
    noPortal?: boolean;
    isOpened?: boolean;
    className?: string;
    isAutoSize?: boolean;
    onCloseRequest?: any;
    style?: IDictionary<string>;
    closeByEsc?: boolean;
    okByEnter?: boolean;
    onKeyDown?: any;
    autofocus?: boolean;
    [key: string]: any;
}
export default Dialog;
