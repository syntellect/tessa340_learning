import * as React from 'react';
declare class IconButton extends React.Component<IIconButtonProps, {}> {
    shouldComponentUpdate(nextProps: any): any;
    render(): JSX.Element;
}
export interface IIconButtonProps {
    children?: any;
    icon?: string;
    className?: string;
    disabled?: boolean;
    disableTouchRipple?: boolean;
    onClick?: any;
    [key: string]: any;
}
export default IconButton;
