import * as React from 'react';
declare class Dropdown extends React.Component<IDropdownProps, {}> {
    shouldComponentUpdate(nextProps: any): boolean;
    render(): JSX.Element;
}
export interface IDropdownProps {
    children?: any;
    className?: string;
    rootElement?: any;
    borderOffset?: number;
    rootOffset?: number;
    horizontalOffset?: number | any;
    onOutsideClick: any;
    openPosition?: 'left' | 'right';
    openDirection?: 'left' | 'right';
    up?: boolean;
    autoDirection?: boolean;
    isOpened: boolean;
    updateStyle?: (style: CSSStyleDeclaration) => void;
    setStyledComponents?: (props: any) => string;
    [key: string]: any;
}
export default Dropdown;
