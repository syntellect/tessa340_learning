import React from 'react';
declare class LabelButton extends React.Component<ILabelButtonProps, {}> {
    static defaultProps: {
        labelPosition: string;
    };
    shouldComponentUpdate(nextProps: any): any;
    render(): JSX.Element;
    focus(opt?: FocusOptions): void;
}
export interface ILabelButtonProps {
    children?: any;
    icon?: string | any;
    className?: string;
    disabled?: boolean;
    disableTouchRipple?: boolean;
    label?: string | null;
    labelPosition?: 'before' | 'after' | null;
    style?: object;
    onClick?: any;
    type?: string;
}
export default LabelButton;
