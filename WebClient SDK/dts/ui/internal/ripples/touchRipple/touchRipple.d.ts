import React from 'react';
declare class TouchRipple extends React.Component<ITouchRippleProps, ITouchRippleState> {
    ignoreNextMouseDown: boolean;
    firstTouchY: number;
    firstTouchX: number;
    startTime: number;
    static defaultProps: {
        color: string;
        opacity: number;
        abortOnScroll: boolean;
    };
    constructor(props: any);
    start(event: any, isRippleTouchGenerated: any): void;
    end(): void;
    getRippleStyle(event: any): {
        directionInvariant: boolean;
        height: number;
        width: number;
        top: number;
        left: number;
    };
    calcDiag(a: any, b: any): number;
    startListeningForScrollAbort(event: TouchEvent): void;
    stopListeningForScrollAbort(): void;
    handleMouseDown: (event: any) => void;
    handleMouseUp: () => void;
    handleMouseLeave: () => void;
    handleTouchStart: (event: any) => void;
    handleTouchEnd: () => void;
    handleTouchMove: (event: any) => void;
    render(): JSX.Element;
}
export interface ITouchRippleProps {
    children?: any;
    color?: string;
    opacity?: number;
    style?: object;
    centerRipple?: boolean;
    abortOnScroll?: boolean;
}
export interface ITouchRippleState {
    hasRipples: boolean;
    nextKey: number;
    ripples: any[];
}
export default TouchRipple;
