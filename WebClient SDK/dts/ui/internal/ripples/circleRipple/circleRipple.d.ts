import * as React from 'react';
declare class CircleRipple extends React.Component<ICircleRipple, {}> {
    enterTimer: any;
    leaveTimer: any;
    static defaultProps: {
        color: string;
        opacity: number;
    };
    shouldComponentUpdate(nextProps: any): any;
    componentWillUnmount(): void;
    componentWillAppear(callback: any): void;
    componentWillEnter(callback: any): void;
    componentDidAppear(): void;
    componentDidEnter(): void;
    componentWillLeave(callback: any): void;
    initializeAnimation(callback: any): void;
    animate(): void;
    render(): JSX.Element;
}
export interface ICircleRipple {
    color?: string;
    opacity?: number;
    style: object;
    aborted?: boolean;
    touchGenerated: boolean;
}
export default CircleRipple;
