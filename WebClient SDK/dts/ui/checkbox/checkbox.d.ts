import * as React from 'react';
declare class Checkbox extends React.Component<CheckboxProps, {}> {
    controlId: string;
    inputRef: HTMLInputElement | null;
    componentWillMount(): void;
    render(): JSX.Element;
    focus(opt?: FocusOptions): void;
}
export interface CheckboxProps {
    disabled?: boolean;
    checked?: boolean;
    caption?: string | null;
    onChange: any;
    className?: string;
    onFocus?: () => void;
    onBlur?: () => void;
    [key: string]: any;
}
export default Checkbox;
