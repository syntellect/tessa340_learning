export interface ICertificate {
    subjectName: string;
    issuerName: string;
    validFrom: string;
    validTo: string;
    company: string;
    serialNumber: string;
}
