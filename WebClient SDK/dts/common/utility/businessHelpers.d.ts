/**
 * Выполняет запрос к серверу и отправляет на логин, если токен неправильный или закончилась сессия
 * @param {(IActionBase) =>      any}         dispatch метод redux
 * @param {string}           path    строка запроса
 * @param {Object}           options параметры запроса
 */
export declare const fetchWithLogoutCheck: (path: any, options?: {}) => Promise<Response>;
/**
 * Выполняет запрос к серверу и отправляет на логин, если токен неправильный или закончилась сессия,
 * и выполняет стандартную обработку
 * @param {IActionBase}         dispatch метод redux
 * @param {string}           path    строка запроса
 * @param {Object}           options параметры запроса
 */
export declare const fetchWithLogoutCheckAndDefaultThen: (path: any, options?: {}) => Promise<any>;
/**
 * Выполняет стандартную обработку ответа от сервера
 * @param  {Promise<any>} promise ответ от сервера
 * @return {Promise<any>}
 */
export declare const processDefaultThen: (promise: Promise<Response>, getResult?: ((reponse: Response) => Promise<any>) | undefined) => Promise<any>;
