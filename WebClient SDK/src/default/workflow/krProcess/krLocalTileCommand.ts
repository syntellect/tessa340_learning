import { IKrTileCommand } from './interfaces';
import { IUIContext, showConfirm } from 'tessa/ui';
import { KrTileInfo, KrProcessInstance, launchProcessWithCardEditor } from 'tessa/workflow/krProcess';
import { Guid } from 'tessa/platform';
import { ICardModel } from 'tessa/ui/cards';
import { CardStoreMode } from 'tessa/cards';
import { ITile } from 'tessa/ui/tiles';

export class KrLocalTileCommand implements IKrTileCommand {

  //#region ctor

  private constructor() {}

  //#endregion

  //#region instance

  private static _instance: KrLocalTileCommand;

  public static get instance(): KrLocalTileCommand {
    if (!KrLocalTileCommand._instance) {
      KrLocalTileCommand._instance = new KrLocalTileCommand();
    }
    return KrLocalTileCommand._instance;
  }

  //#endregion

  public async onClickAction(
    context: IUIContext,
    _tile: ITile,
    tileInfo: KrTileInfo
  ) {
    const cardEditor = context.cardEditor;
    let model: ICardModel;
    if (tileInfo.id === Guid.empty
      || !cardEditor
      || !(model = cardEditor.cardModel!)
    ) {
      return;
    }

    if (tileInfo.askConfirmation) {
      const result = await showConfirm(tileInfo.confirmationMessage);
      if (!result) {
        return;
      }
    }

    await cardEditor.setOperationInProgress(async () => {
      if (model.card.storeMode === CardStoreMode.Insert) {
        const result = await cardEditor.saveCard(context);
        if (!result
          || !context.cardEditor
          || !context.cardEditor.lastData.storeResponse
          || !context.cardEditor.lastData.storeResponse.validationResult.isSuccessful
        ) {
          return;
        }
      }

      const process = new KrProcessInstance(
        tileInfo.id,
        context.cardEditor!.cardModel!.card.id,
        {},
        [ 'UseCurrentCardEditor' ]
      );

      await launchProcessWithCardEditor(process, cardEditor);
    });
  }
}