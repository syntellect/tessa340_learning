import {
  ApplicationExtension,
  IApplicationExtensionMetadataContext,
  MetadataStorage,
  WorkspaceStorage
} from 'tessa';
import { DeskiMetadata, Result, DeskiInfo } from 'tessa/platform';
import { MenuAction } from 'tessa/ui';
import { FileExtension, FileExtensionContext } from 'tessa/ui/files';
import { IFile, FileContainer } from 'tessa/files';
import { ValidationResultBuilder, ValidationResultType } from 'tessa/platform/validation';
import { CardUIExtension, ICardUIExtensionContext } from 'tessa/ui/cards';
import { LocalizationManager } from 'tessa/localization';

const InfoKey = '.deskiExtension';
let DeskiLock = false;

export class DeskiExtension extends ApplicationExtension {
  public afterMetadataReceived(_context: IApplicationExtensionMetadataContext) {
    if (!MetadataStorage.instance.commonMetadata.deskiInfo.enabled) {
      return;
    }
    if (DeskiLock) {
      console.log("Another deski's extension is already searching for its host companion!");
      return;
    }
    DeskiLock = true;
    findDeski().then(resp => {
      if (resp.port) {
        MetadataStorage.instance.setCommonMetadataDeskiMeta(
          new DeskiMetadata({ enabled: true, ...resp })
        );
      }
    }).finally(() => DeskiLock = false);
  }
}

export class DeskiFileExtension extends FileExtension {
  public openingMenu(context: FileExtensionContext) {
    if (
      !MetadataStorage.instance.commonMetadata.deskiInfo.enabled ||
      !MetadataStorage.instance.commonMetadata.deskiInfo.port
    ) {
      return;
    }

    const file = context.file.model;

    const editCollapsed = !file.permissions.canEdit;
    context.actions.splice(
      0,
      0,
      new MenuAction(
        'EditFile',
        '$UI_Controls_FilesControl_OpenForEdit',
        'ta icon-thin-002',
        async () => {
          let item: DeskiItem;
          if (!file.lastVersion.info[InfoKey] || !(item = file.lastVersion.info[InfoKey])) {
            item = new DeskiItem(file.lastVersion.name, file.lastVersion.id);
            file.lastVersion.info[InfoKey] = item;
          }

          context.file.model.isDirty = true;

          if (item.Status == DeskiStatus.Open) {
            await openInDeskiServer(file.lastVersion.id);
            return;
          }
          if (item.Status == DeskiStatus.SavedWithError) {
            alert("System hasn't saved the file yet!");
            return;
          }
          await editInDeskiServer(file, item);
        },
        null,
        editCollapsed
      )
    );
  }
}

export class DeskiUIExtension extends CardUIExtension {
  shouldExecute() {
    if (
      !MetadataStorage.instance.commonMetadata.deskiInfo.enabled ||
      !MetadataStorage.instance.commonMetadata.deskiInfo.port
    ) {
      return false;
    }
    return true;
  }
  public async saving(context: ICardUIExtensionContext) {
    let files = context.fileContainer.files;
    if (!files) {
      return;
    }

    const openFiles = (await loadOpenFiles()).filter(x =>
      files.some(y => y.lastVersion.id === x.ID)
    );
    const lockedFiles = openFiles.filter(x => x.IsLocked);
    if (lockedFiles.length > 0) {
      lockedFiles.forEach(x =>
        context.validationResult.add(
          ValidationResultType.Error,
          LocalizationManager.instance.format('$UI_Cards_FileSaving_Locked', x.Name)
        )
      );
      return;
    }

    for (let file of files) {
      const item: DeskiItem | undefined = file.lastVersion.info[InfoKey];
      if (
        !item ||
        (item.Status !== DeskiStatus.Open && item.Status !== DeskiStatus.SavedWithError)
      ) {
        continue;
      }

      try {
        const res = openFiles.find(x => (x.ID = file.lastVersion.id));
        if (!res) {
          continue;
        }
        if (!res.IsModified) {
          console.log('not modified!');
          item.Status = DeskiStatus.Default;
          await removeFromDeskiServer(item);
          continue;
        }
        item.IsModified = true;
        await saveToRemoteServer(item, file);

        delete file.lastVersion.info[InfoKey];
      } catch (error) {
        item.Message = getEchoErrorMessage(error);
        item.Status = DeskiStatus.SavedWithError;
      }
    }
  }
  public async finalized(context: ICardUIExtensionContext) {
    await clearEditedFiles(context.fileContainer);
  }
}

const openInDeskiServer = async (fileId: string) => {
  const url = new URL(`${getDeskiServerUrl()}/open`);
  url.searchParams.append('id', fileId);
  await fetch(url.href);
};

const editInDeskiServer = async (file: IFile, item: DeskiItem) => {
  let content: Result<File>;
  if (file.lastVersion.content) {
    content = {
      data: file.lastVersion.content,
      validationResult: new ValidationResultBuilder().build()
    };
  } else {
    content = await file.source.getContent(file);
  }

  if (!content.data) {
    item.Message = '$Deski_Cant_Load_Content';
    item.Status = DeskiStatus.OpenWithError;
    return;
  }
  try {
    const options: RequestInit = {
      method: 'POST',
      credentials: 'same-origin'
    };

    const formData = new FormData();
    formData.append('editAsName', file.lastVersion.name);
    formData.append('id', file.lastVersion.id);
    formData.append('file', new Blob([content.data]));
    options.body = formData;

    const headers = new Headers();
    options.headers = headers;

    await fetch(`${getDeskiServerUrl()}/edit`, options);
    item.Message = '$Deski_File_Edition';
    item.Status = DeskiStatus.Open;
  } catch (error) {
    item.Message = getEchoErrorMessage(error);
    item.Status = DeskiStatus.OpenWithError;

    console.log(item.Message);
  }
};

// при закрытии вкладки или обновлении страницы вручную надо удалить окрытые файлы
window.addEventListener('beforeunload', function() {
  const cards = WorkspaceStorage.instance.cards;
  if (cards.size === 0) {
    return;
  }

  for (let card of cards.values()) {
    if (card.editor.cardModel) {
      clearEditedFiles(card.editor.cardModel.fileContainer);
    }
  }
});

class DeskiItem {
  public Name: string;
  public ID: string;
  public IsModified: boolean;
  public IsLocked: boolean;
  public Message: string;
  public Status: DeskiStatus;
  constructor(fileName, fileId) {
    this.Name = fileName;
    this.ID = fileId;
    this.IsModified = false;
    this.IsLocked = false;
    this.Message = '';
    this.Status = DeskiStatus.Default;
  }
}

enum DeskiStatus {
  Default = 1,
  Open = 2,
  Saved = 3,
  SavedWithError = 4,
  OpenWithError = 5
}

const DESKI_SERVER_PATH = 'http://127.0.0.1';


async function findDeski() {
  let port = 7711;
  const maxIterations = 20;
  let response;
  const shifts = stringToArray('TESSA');
  let deskiPort: number | undefined = 0;
  let info: DeskiInfo | undefined;

  for (let i = 0; port < 65535 && i < maxIterations; i++) {
    try {
      console.log(`trying port ${port}`);
      response = await (await fetchWithTimeout(
        1000,
        fetch(`${DESKI_SERVER_PATH}:${port}/tessa-deski-info`)
      )).text();
      try {
        const resp: DeskiInfo = JSON.parse(response);
        if (resp && resp.ShortName === 'deski') {
          console.log('yahooooo!');
          deskiPort = port;
          info = resp;
          console.log(getDeskiServerUrl(deskiPort));
          return { port: deskiPort, info };
        } else {
          console.log("someone responded, but that's not deski (");
        }
      } catch (err) {
        console.log("someone responded, but that's not deski (");
      }
    } catch (error) {
      console.log(error);
    }
    port += shifts[i % 5];
  }

  return { port: deskiPort, info };
}

function stringToArray(str: string) {
  const arr: number[] = [];
  for (let i = 0; i < str.length; i++) {
    arr.push(str.charCodeAt(i));
  }
  return arr;
}

function getDeskiServerUrl(deskiPort?: number) {
  return `${DESKI_SERVER_PATH}:${deskiPort ||
    MetadataStorage.instance.commonMetadata.deskiInfo.port ||
    0}`;
}

function fetchWithTimeout(ms: number, promise: Promise<any>): Promise<any> {
  return new Promise(function(resolve, reject) {
    var didTimeOut = false;
    const timeout = setTimeout(function() {
      didTimeOut = true;
      reject(new Error('Request timed out'));
    }, ms);

    promise
      .then(function(response) {
        // Clear the timeout as cleanup
        clearTimeout(timeout);
        if (!didTimeOut) {
              //console.log('fetch good! ', response);
          resolve(response);
        }
      })
      .catch(function(err) {
          //console.log('fetch failed! ', err);
          clearTimeout(timeout);
        // Rejection already happened with setTimeout
        if (didTimeOut) return;
        // Reject with error
        reject(err);
      });
  });
}

function getEchoErrorMessage(err) {
  return err.toString();
}

const clearEditedFiles = async (fileContainer: FileContainer) => {
  const files = fileContainer.files;
  if (!files) {
    return;
  }

  for (let file of files) {
    const item: DeskiItem | undefined = file.lastVersion.info[InfoKey];
    if (!item || (item.Status !== DeskiStatus.Open && item.Status !== DeskiStatus.SavedWithError)) {
      continue;
    }

    try {
      await removeFromDeskiServer(item);
      item.IsModified = false;

      delete file.lastVersion.info[InfoKey];
    } catch (error) {
      item.Message = getEchoErrorMessage(error);
      item.Status = DeskiStatus.SavedWithError;
    }
  }
};

// const getItemFromDeskiServer = async (item: DeskiItem) => {
//   const url = new URL(`${getDeskiServerUrl()}/item`);
//   url.searchParams.append('id', item.id);
//   const response = await fetch(url.href);
//   return await response.json();
// };

const removeFromDeskiServer = async (item: DeskiItem) => {
  const url = new URL(`${getDeskiServerUrl()}/remove`);
  url.searchParams.append('id', item.ID);

  await fetch(url.href);
  console.log(
    `removing file [ ${item.ID}(${item.Name})] from local service completed successfully`
  );
};

const saveToRemoteServer = async (item: DeskiItem, file: IFile) => {
  const url = new URL(`${getDeskiServerUrl()}/file`);
  url.searchParams.append('id', item.ID);

  const blob = await (await fetch(url.href)).blob();

  file.replace(new File([blob], item.Name), true);

  item.IsModified = false;
  file.isDirty = false;
  removeFromDeskiServer(item);
  item.Message = '$Deski_successuful_save';
  item.Status = DeskiStatus.Saved;
};

const loadOpenFiles: () => Promise<DeskiItem[]> = async () => {
  const url = new URL(`${getDeskiServerUrl()}/list`);

  try {
    return (await (await fetch(url.href)).json()) || [];
  } catch {
    return [];
  }
};
