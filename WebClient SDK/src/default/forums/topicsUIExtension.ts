import { reaction } from 'mobx';
import { CardUIExtension, ICardUIExtensionContext, ICardModel } from 'tessa/ui/cards';
import { ForumViewModel, CheckBoxViewModel, ButtonViewModel } from 'tessa/ui/cards/controls';
import { DefaultFormMainViewModel } from 'tessa/ui/cards/forms';
import { MetadataStorage } from 'tessa';
import { CardNewRequest, CardService } from 'tessa/cards/service';
import { showNotEmpty, createCardModel, UIButton, showLoadingOverlay } from 'tessa/ui';
import { CardFieldChangedEventArgs, Card, CardTableType } from 'tessa/cards';
import { showFormDialog } from 'tessa/ui/uiHost';
import { ForumProvider, TopicModel, ParticipantTypes } from 'tessa/forums';
import { userSession } from 'common/utility';
import { ValidationResult, ValidationResultType } from 'tessa/platform/validation';

export class TopicsUIExtension  extends CardUIExtension {

  //#region fields

  private _context: ICardUIExtensionContext | null = null;

  private _forumViewModel: ForumViewModel | null = null;

  private _participants: guid[] | null = null;

  private _participantReadOnly: boolean = false;

  private _participantModeratorMode: boolean = false;

  private _dispose: Function | null = null;

  //#endregion

  //#region CardUIExtension

  public initialized(context: ICardUIExtensionContext) {
    const forumTab = context.model.forms.find(x => x.name === 'Forum');
    if (!forumTab) {
      return;
    }

    const topicsBlock = forumTab.blocks.find(x => x.name === 'Topics');
    if (!topicsBlock) {
      return;
    }

    const forumControl = topicsBlock.controls[0] as ForumViewModel;
    if (!forumControl
      || !(forumControl instanceof ForumViewModel)
    ) {
      return;
    }

    if (!forumControl.isLicenseValid) {
      return;
    }

    const mainForm = context.model.mainForm as DefaultFormMainViewModel;
    if (!mainForm
      || !(mainForm instanceof DefaultFormMainViewModel)
    ) {
      return;
    }

    this._context = context;
    this._forumViewModel = forumControl;
    this.initForumControl(forumControl);

    if (mainForm.selectedTab === forumTab) {
      forumControl.showTopics();
    } else {
      this._dispose = reaction(
        () => mainForm.selectedTab,
        selectedTab => {
          if (selectedTab === forumTab && !forumControl.content) {
            forumControl.showTopics();
            if (this._dispose) {
              this._dispose();
              this._dispose = null;
            }
          }
        }
      );
    }
  }

  public finalized() {
    this._participantReadOnly = false;
    this._participantModeratorMode = false;
    this._participants = null;
    this._forumViewModel = null;
    this._context = null;
    if (this._dispose) {
      this._dispose();
      this._dispose = null;
    }
  }

  //#endregion

  //#region methods

  private initForumControl(control: ForumViewModel) {
    control.addTopicAction = this.addTopicAction;
    control.addParticipantsAction = this.addParticipantsAction;
    control.addRoleParticipantsAction = this.addRoleParticipantsAction;
    control.openParticipantsAction = this.openParticipantsAction;
    control.checkAddTopicPermissionAction = this.checkAddTopicPermissionsAction;
    control.checkSuperModeratorPermissionAction = this.checkSuperModeratorPermissionAction;
  }

  private disableCheckBox = (e: CardFieldChangedEventArgs, model: ICardModel) => {
    if (e.fieldName === 'ReadOnly') {
      const control = model.controls.get('IsModerator') as CheckBoxViewModel;
      if (control) {
        control.isChecked = false;
        control.isReadOnly = e.fieldValue;
      }
    }
    if (e.fieldName === 'IsModerator') {
      const control = model.controls.get('IsReadOnly') as CheckBoxViewModel;
      if (control) {
        control.isChecked = false;
        control.isReadOnly = e.fieldValue;
      }
    }
  }

  private addParticipantsShowDialogWithoutTopicId = async (): Promise<void> => {
    const dialogType = MetadataStorage.instance.cardMetadata.getCardTypeById(
      'aed972eb-883d-4b02-a75b-c96d4a5aef4c'); // TopicTabTypeID
    if (dialogType) {
      const request = new CardNewRequest();
      request.cardTypeId = 'aed972eb-883d-4b02-a75b-c96d4a5aef4c'; // TopicTabTypeID
      const response = await CardService.instance.new(request);
      const result = response.validationResult.build();
      if (!result.isSuccessful) {
        await showNotEmpty(result);
        return;
      }

      const model = createCardModel(response.card, response.sectionRows);
      const dialogForm = dialogType.forms.find(x => x.name === 'AddParticipantsTabName');

      if (dialogForm) {
        await showFormDialog(
          dialogForm,
          model,
          null,
          [
            UIButton.create({
              caption: '$Forum_Dialog_Button_AddParticipants',
              buttonAction: b => {
                this.getParticipants(model);
                b.close();
              }
            }),
            UIButton.create({
              caption: '$Forum_Dialog_Button_Cancel',
              buttonAction: b => {
                this._participants = null;
                b.close()
              }
            })
          ]
        );
      }
    }
  }

  private saveTopics = async (
    context: ICardUIExtensionContext,
    model: ICardModel
  ): Promise<boolean> => {
    return await showLoadingOverlay(async () => {
      const title = model.card.sections.get('FmAddTopicInfoVirtual')!.fields.get('Title');
      const description = model.card.sections.get('FmAddTopicInfoVirtual')!.fields.get('Description');

      if (!title) {
        await showNotEmpty(
          ValidationResult.fromText(
            'Title is required.', // TODO: localize
            ValidationResultType.Warning
          )
        );
        return false;
      }

      this._participants = this.getParticipantsList(model.card, 'FmTopicParticipantsVirtual');
      if (this._participants.length > 0) {
        const participantsInfo = model.card.sections.get('FmTopicParticipantsInfoVirtual');
        if (participantsInfo) {
          const readOnly = participantsInfo.fields.get('ReadOnly');
          const moderator = participantsInfo.fields.get('IsModerator');
          this._participantReadOnly = !!readOnly;
          this._participantModeratorMode = !!moderator;
        }
      }

      const topicModel = new TopicModel();
      topicModel.title = title;
      topicModel.description = description;
      topicModel.authorId = userSession.UserID;

      const response = await ForumProvider.instance.addTopic(
        context.card.id,
        topicModel
      );

      const result = response.validationResult.build();
      if (result.isSuccessful) {
        const topic = new TopicModel(response.info);
        await this.saveAddParticipants(model, topic.id);
        await this._forumViewModel!.showTopic(topic.id);

        model.card.sections.get('FmAddTopicInfoVirtual')!.fields.set('Title', null);
        model.card.sections.get('FmAddTopicInfoVirtual')!.fields.set('Description', null);
      } else {
        await showNotEmpty(result);
        return false;
      }

      return true;
    });
  }

  private getParticipants = (
    model: ICardModel
  ) => {
    if (!this._participants) {
      return;
    }

    this._participants = this.getParticipantsList(model.card, 'FmTopicParticipantsVirtual');
    if (this._participants.length > 0) {
      const participantsInfo = model.card.sections.get('FmTopicParticipantsInfoVirtual');
      if (participantsInfo) {
        const readOnly = participantsInfo.fields.get('ReadOnly');
        const moderator = participantsInfo.fields.get('IsModerator');
        this._participantReadOnly = !!readOnly;
        this._participantModeratorMode = !!moderator;
      }
    }
  }

  private getParticipantsList(
    card: Card,
    sectionName: string
  ): guid[] {
    const participants: guid[] = [];
    const section = card.sections.get(sectionName);
    if (section) {
      section.tableType = CardTableType.Collection; // TODO: ???
      for (let row of section.rows) {
        participants.push(row.get('ParticipantID'));
      }
    }
    return participants;
  }

  private saveAddParticipants = async (
    model: ICardModel,
    topicId: guid
  ): Promise<void> => {
    await showLoadingOverlay(async () => {
      this.getParticipants(model);
      if (this._participants && this._participants.length > 0) {
        const response = await ForumProvider.instance.addParticipants(
          topicId,
          this._participants,
          this._participantReadOnly,
          this._participantModeratorMode ? ParticipantTypes.Moderator : ParticipantTypes.Participant,
          false,
          false
        );

        const result = response.validationResult.build();
        if (result.isSuccessful) {
          this._forumViewModel!.refresh();
        } else {
          await showNotEmpty(result);
        }
      }
    });

    this._participants = null;
  }

  //#endregion

  //#region actions

  private addTopicAction = async (): Promise<void> => {
    const dialogType = MetadataStorage.instance.cardMetadata.getCardTypeById(
      'aed972eb-883d-4b02-a75b-c96d4a5aef4c'); // TopicTabTypeID
    if (dialogType) {
      const request = new CardNewRequest();
      request.cardTypeId = 'aed972eb-883d-4b02-a75b-c96d4a5aef4c'; // TopicTabTypeID
      const response = await CardService.instance.new(request);
      const result = response.validationResult.build();
      if (!result.isSuccessful) {
        await showNotEmpty(result);
        return;
      }

      const model = createCardModel(response.card, response.sectionRows);
      const dialogForm = dialogType.forms.find(x => x.name === 'AddTopicTabName')!;

      model.card.sections.get('FmTopicParticipantsInfoVirtual')!.fields.fieldChanged.add(
        e => this.disableCheckBox(e, model)
      );

      await showFormDialog(
        dialogForm,
        model,
        (f) => {
          const buttonAddParticipants = f.blocks[0].controls.find(x => x.name === 'AddParticipants') as ButtonViewModel;
          if (buttonAddParticipants) {
            buttonAddParticipants.onClick = this.addParticipantsShowDialogWithoutTopicId;
          }
        },
        [
          UIButton.create({
            caption: '$Forum_Dialog_Button_Save',
            buttonAction: async b => {
              if (await this.saveTopics(this._context!, model)) {
                b.close();
              }
            }
          }),
          UIButton.create({
            caption: '$Forum_Dialog_Button_Cancel',
            buttonAction: b => b.close()
          })
        ]
      );
    }
  }

  private addParticipantsAction = async (): Promise<void> => {

  }

  private addRoleParticipantsAction = async (): Promise<void> => {

  }

  private openParticipantsAction = async (): Promise<void> => {

  }

  private checkAddTopicPermissionsAction = async (): Promise<void> => {

  }

  private checkSuperModeratorPermissionAction = async (): Promise<void> => {

  }

  //#endregion

}