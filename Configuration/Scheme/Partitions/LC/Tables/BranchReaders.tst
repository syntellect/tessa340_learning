﻿<?xml version="1.0" encoding="utf-8"?>
<SchemeTable Partition="7ee766f0-f37b-498d-bea3-d96f09e287e7" ID="7b143931-dc50-4a50-bd76-a75ac44750ac" Name="BranchReaders" Group="LC" InstanceType="Cards" ContentType="Collections">
	<SchemeComplexColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="7b143931-dc50-0050-2000-075ac44750ac" Name="ID" Type="Reference(Typified) Not Null" ReferencedTable="1074eadd-21d7-4925-98c8-40d1e5f0ca0e">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="7b143931-dc50-0150-4000-075ac44750ac" Name="ID" Type="Guid Not Null" ReferencedColumn="9a58123b-b2e9-4137-9c6c-5dab0ec02747" />
	</SchemeComplexColumn>
	<SchemePhysicalColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="7b143931-dc50-0050-3100-075ac44750ac" Name="RowID" Type="Guid Not Null" />
	<SchemeComplexColumn ID="ddc3a50f-c35c-4368-ac0c-27bcf566e0d1" Name="Role" Type="Reference(Typified) Null" ReferencedTable="81f6010b-9641-4aa5-8897-b8e8603fbf4b">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="ddc3a50f-c35c-0068-4000-07bcf566e0d1" Name="RoleID" Type="Guid Null" ReferencedColumn="81f6010b-9641-01a5-4000-08e8603fbf4b" />
		<SchemeReferencingColumn ID="5397de05-3024-4c2b-afa9-d5f0e8aad20d" Name="RoleName" Type="String(128) Null" ReferencedColumn="616d6b2e-35d5-424d-846b-618eb25962d0" />
	</SchemeComplexColumn>
	<SchemePrimaryKey IsSystem="true" IsPermanent="true" IsSealed="true" ID="7b143931-dc50-0050-5000-075ac44750ac" Name="pk_BranchReaders">
		<SchemeIndexedColumn Column="7b143931-dc50-0050-3100-075ac44750ac" />
	</SchemePrimaryKey>
	<SchemeIndex IsSystem="true" IsPermanent="true" IsSealed="true" ID="7b143931-dc50-0050-7000-075ac44750ac" Name="idx_BranchReaders_ID" IsClustered="true">
		<SchemeIndexedColumn Column="7b143931-dc50-0150-4000-075ac44750ac" />
	</SchemeIndex>
</SchemeTable>