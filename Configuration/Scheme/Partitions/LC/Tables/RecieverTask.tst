﻿<?xml version="1.0" encoding="utf-8"?>
<SchemeTable Partition="7ee766f0-f37b-498d-bea3-d96f09e287e7" ID="8fe359d3-06c0-4068-bb73-adabec0c9059" Name="RecieverTask" InstanceType="Tasks" ContentType="Entries">
	<SchemeComplexColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="8fe359d3-06c0-0068-2000-0dabec0c9059" Name="ID" Type="Reference(Typified) Not Null" ReferencedTable="5bfa9936-bb5a-4e8f-89a9-180bfd8f75f8">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="8fe359d3-06c0-0168-4000-0dabec0c9059" Name="ID" Type="Guid Not Null" ReferencedColumn="5bfa9936-bb5a-008f-3100-080bfd8f75f8" />
	</SchemeComplexColumn>
	<SchemeComplexColumn ID="c2610c17-6e42-45e7-b3be-d3647f70943d" Name="Controller" Type="Reference(Typified) Null" ReferencedTable="6c977939-bbfc-456f-a133-f1c2244e3cc3">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="c2610c17-6e42-00e7-4000-03647f70943d" Name="ControllerID" Type="Guid Null" ReferencedColumn="6c977939-bbfc-016f-4000-01c2244e3cc3" />
		<SchemeReferencingColumn ID="f216f74e-97d7-4ae4-b677-e1fb4c3fa7a3" Name="ControllerName" Type="String(128) Null" ReferencedColumn="1782f76a-4743-4aa4-920c-7edaee860964" />
	</SchemeComplexColumn>
	<SchemePhysicalColumn ID="0f20bae8-4fb9-4817-bbf9-3ae88a08e868" Name="Comment" Type="String(Max) Null" />
	<SchemePrimaryKey IsSystem="true" IsPermanent="true" IsSealed="true" ID="8fe359d3-06c0-0068-5000-0dabec0c9059" Name="pk_RecieverTask" IsClustered="true">
		<SchemeIndexedColumn Column="8fe359d3-06c0-0168-4000-0dabec0c9059" />
	</SchemePrimaryKey>
</SchemeTable>