﻿<?xml version="1.0" encoding="utf-8"?>
<SchemeTable Partition="7a2d63ec-5ea5-42b6-b0d3-d6ea2c1929a8" ID="72fd5289-c709-4f93-ba89-a2aca8a348f9" Name="DmRequestedAdditionalApprovalInfoVirtual" Group="DmStageTypes" IsVirtual="true" InstanceType="Tasks" ContentType="Collections">
	<SchemeComplexColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="72fd5289-c709-0093-2000-02aca8a348f9" Name="ID" Type="Reference(Typified) Not Null" ReferencedTable="5bfa9936-bb5a-4e8f-89a9-180bfd8f75f8">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="72fd5289-c709-0193-4000-02aca8a348f9" Name="ID" Type="Guid Not Null" ReferencedColumn="5bfa9936-bb5a-008f-3100-080bfd8f75f8" />
	</SchemeComplexColumn>
	<SchemePhysicalColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="72fd5289-c709-0093-3100-02aca8a348f9" Name="RowID" Type="Guid Not Null" />
	<SchemeComplexColumn ID="3e53fe42-6150-42af-8858-d9955eded631" Name="Performer" Type="Reference(Typified) Not Null" ReferencedTable="81f6010b-9641-4aa5-8897-b8e8603fbf4b" WithForeignKey="false">
		<Description>Роль, на которую назначено задание.&#xD;
Может быть временной ролью, которая удалится после завершения задания.</Description>
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="3e53fe42-6150-00af-4000-09955eded631" Name="PerformerID" Type="Guid Not Null" ReferencedColumn="81f6010b-9641-01a5-4000-08e8603fbf4b" />
		<SchemeReferencingColumn ID="b60a80fe-e1e4-458c-b564-c022d0653520" Name="PerformerName" Type="String(128) Not Null" ReferencedColumn="616d6b2e-35d5-424d-846b-618eb25962d0" />
	</SchemeComplexColumn>
	<SchemeComplexColumn ID="9edaef40-a541-4b05-80df-52800e06f45f" Name="User" Type="Reference(Typified) Null" ReferencedTable="6c977939-bbfc-456f-a133-f1c2244e3cc3">
		<Description>Пользователь, который взял задание в работу или завершил его, или Null, если задание было создано, но не было взято в работу.</Description>
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="9edaef40-a541-0005-4000-02800e06f45f" Name="UserID" Type="Guid Null" ReferencedColumn="6c977939-bbfc-016f-4000-01c2244e3cc3" />
		<SchemeReferencingColumn ID="833e2b59-1946-4a04-af8e-7c38581c92d8" Name="UserName" Type="String(128) Null" ReferencedColumn="1782f76a-4743-4aa4-920c-7edaee860964" />
	</SchemeComplexColumn>
	<SchemeComplexColumn ID="cc77ceab-f4cc-47ee-b4f9-681b5c6f66c6" Name="Option" Type="Reference(Typified) Null" ReferencedTable="08cf782d-4130-4377-8a49-3e201a05d496">
		<Description>Вариант завершения задания или Null, если задание ещё не было завершено.</Description>
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="cc77ceab-f4cc-00ee-4000-081b5c6f66c6" Name="OptionID" Type="Guid Null" ReferencedColumn="132dc5f5-ce87-4dd0-acce-b4a02acf7715" />
		<SchemeReferencingColumn ID="53bad3cc-fa41-4e98-80fb-35e298ef7cd4" Name="OptionCaption" Type="String(128) Null" ReferencedColumn="6762309a-b0ff-4b2f-9cce-dd111116e554" />
	</SchemeComplexColumn>
	<SchemePhysicalColumn ID="bfa2377b-795f-4a86-8664-9b9d25e94296" Name="Comment" Type="String(Max) Null">
		<Description>Комментарий к заданию доп. согласования. Например, вопрос, который был задан.</Description>
	</SchemePhysicalColumn>
	<SchemePhysicalColumn ID="0997e373-c601-4b1b-bbff-cee8cf758f2f" Name="Answer" Type="String(Max) Null">
		<Description>Ответный комментарий на задание доп. согласования или Null, если комментарий ещё не был указан.</Description>
	</SchemePhysicalColumn>
	<SchemePhysicalColumn ID="0097598c-8652-4c31-b64d-a6fc68436134" Name="Created" Type="DateTime Not Null">
		<Description>Дата и время, когда отправлено задание.</Description>
	</SchemePhysicalColumn>
	<SchemePhysicalColumn ID="3f90f073-ee04-4206-b843-0a9583c0ea4d" Name="InProgress" Type="DateTime Null">
		<Description>Дата взятия задания в работу или Null, если резолюция ещё не была взята в работу.</Description>
	</SchemePhysicalColumn>
	<SchemePhysicalColumn ID="5734fef0-d95f-43e6-8a85-0cd5c0a5a5f7" Name="Planned" Type="DateTime Not Null">
		<Description>Дата и время запланированного завершения задания.</Description>
	</SchemePhysicalColumn>
	<SchemePhysicalColumn ID="984c71d6-7fcd-4d36-ab22-0a447378eea1" Name="Completed" Type="DateTime Null">
		<Description>Дата завершения задания или Null, если задание ещё не было завершено.</Description>
	</SchemePhysicalColumn>
	<SchemePhysicalColumn ID="d970a7d9-b9ba-44b3-8512-10c48aa288bd" Name="ColumnComment" Type="String(Max) Null">
		<Description>Краткий комментарий к заданию доп. согласования.&#xD;
Выводится в колонке таблицы для заданий доп. согласования.</Description>
	</SchemePhysicalColumn>
	<SchemePhysicalColumn ID="e66dbb32-080f-46aa-9353-3b4e3068277d" Name="ColumnState" Type="String(Max) Null">
		<Description>Краткая информация по текущему заданию доп. согласования.&#xD;
Выводится в колонке таблицы для заданий доп. согласования.</Description>
	</SchemePhysicalColumn>
	<SchemePrimaryKey IsSystem="true" IsPermanent="true" IsSealed="true" ID="72fd5289-c709-0093-5000-02aca8a348f9" Name="pk_DmRequestedAdditionalApprovalInfoVirtual">
		<SchemeIndexedColumn Column="72fd5289-c709-0093-3100-02aca8a348f9" />
	</SchemePrimaryKey>
	<SchemeIndex IsSystem="true" IsPermanent="true" IsSealed="true" ID="72fd5289-c709-0093-7000-02aca8a348f9" Name="idx_DmRequestedAdditionalApprovalInfoVirtual_ID" IsClustered="true">
		<SchemeIndexedColumn Column="72fd5289-c709-0193-4000-02aca8a348f9" />
	</SchemeIndex>
</SchemeTable>