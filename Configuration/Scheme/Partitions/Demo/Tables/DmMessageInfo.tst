﻿<?xml version="1.0" encoding="utf-8"?>
<SchemeTable Partition="7a2d63ec-5ea5-42b6-b0d3-d6ea2c1929a8" ID="b86d9818-c7ec-491d-b583-fa7e77f8903f" Name="DmMessageInfo" Group="Dm" InstanceType="Cards" ContentType="Entries">
	<Description>Секция карточки поручения</Description>
	<SchemeComplexColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="b86d9818-c7ec-001d-2000-0a7e77f8903f" Name="ID" Type="Reference(Typified) Not Null" ReferencedTable="1074eadd-21d7-4925-98c8-40d1e5f0ca0e">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="b86d9818-c7ec-011d-4000-0a7e77f8903f" Name="ID" Type="Guid Not Null" ReferencedColumn="9a58123b-b2e9-4137-9c6c-5dab0ec02747" />
	</SchemeComplexColumn>
	<SchemeComplexColumn ID="49691e5a-d959-4c19-a49c-e78e900d1dde" Name="BaseCard" Type="Reference(Abstract) Not Null">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="49691e5a-d959-0019-4000-078e900d1dde" Name="BaseCardID" Type="Guid Not Null" ReferencedColumn="9a58123b-b2e9-4137-9c6c-5dab0ec02747" />
		<SchemePhysicalColumn ID="68168a61-337d-417b-9b2c-eed648024bf4" Name="BaseCardName" Type="String(Max) Not Null" />
	</SchemeComplexColumn>
	<SchemeComplexColumn ID="c08db252-1eca-4562-9611-12aca2d90e13" Name="ParentMessage" Type="Reference(Typified) Null" ReferencedTable="b86d9818-c7ec-491d-b583-fa7e77f8903f">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="c08db252-1eca-0062-4000-02aca2d90e13" Name="ParentMessageID" Type="Guid Null" ReferencedColumn="b86d9818-c7ec-011d-4000-0a7e77f8903f" />
		<SchemePhysicalColumn ID="d12bffa4-21d5-43c0-8e2d-1defed8f2961" Name="ParentMessageName" Type="String(Max) Null" />
	</SchemeComplexColumn>
	<SchemeComplexColumn ID="faf879e2-9b62-4094-b6b8-72cf7ca20ed0" Name="Controler" Type="Reference(Typified) Null" ReferencedTable="6c977939-bbfc-456f-a133-f1c2244e3cc3">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="faf879e2-9b62-0094-4000-02cf7ca20ed0" Name="ControlerID" Type="Guid Null" ReferencedColumn="6c977939-bbfc-016f-4000-01c2244e3cc3" />
		<SchemeReferencingColumn ID="042f2136-8bb8-4426-9dd7-b3d8b61b1300" Name="ControlerName" Type="String(128) Null" ReferencedColumn="1782f76a-4743-4aa4-920c-7edaee860964" />
	</SchemeComplexColumn>
	<SchemeComplexColumn ID="cff68992-4bc9-4717-87c6-70187f4b6114" Name="Responsible" Type="Reference(Typified) Not Null" ReferencedTable="6c977939-bbfc-456f-a133-f1c2244e3cc3">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="cff68992-4bc9-0017-4000-00187f4b6114" Name="ResponsibleID" Type="Guid Not Null" ReferencedColumn="6c977939-bbfc-016f-4000-01c2244e3cc3" />
		<SchemeReferencingColumn ID="1a198f37-5926-4622-8979-c810f0fce33e" Name="ResponsibleName" Type="String(128) Not Null" ReferencedColumn="1782f76a-4743-4aa4-920c-7edaee860964" />
	</SchemeComplexColumn>
	<SchemePhysicalColumn ID="a5bde11e-936d-4724-93f1-3207ee807717" Name="Planned" Type="Date Not Null" />
	<SchemePhysicalColumn ID="5bfaf387-6280-40eb-b106-17c319c8f81d" Name="Completed" Type="DateTime Null" />
	<SchemePhysicalColumn ID="5c2b2730-e78e-4cd1-9466-79375d88c4c9" Name="TaskID" Type="Guid Null">
		<Description>Идентификатор задания, которое создало данное поручение.</Description>
	</SchemePhysicalColumn>
	<SchemePrimaryKey IsSystem="true" IsPermanent="true" IsSealed="true" ID="b86d9818-c7ec-001d-5000-0a7e77f8903f" Name="pk_DmMessageInfo" IsClustered="true">
		<SchemeIndexedColumn Column="b86d9818-c7ec-011d-4000-0a7e77f8903f" />
	</SchemePrimaryKey>
</SchemeTable>