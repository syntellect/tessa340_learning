﻿<?xml version="1.0" encoding="utf-8"?>
<SchemeTable Partition="7a2d63ec-5ea5-42b6-b0d3-d6ea2c1929a8" ID="06146160-2120-4c4b-8437-7ea3d9d6666e" Name="DmTaskChildMessagesVirtual" Group="Dm" IsVirtual="true" InstanceType="Tasks" ContentType="Collections">
	<Description>Секция с информацией о дочерних поручениях</Description>
	<SchemeComplexColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="06146160-2120-004b-2000-0ea3d9d6666e" Name="ID" Type="Reference(Typified) Not Null" ReferencedTable="5bfa9936-bb5a-4e8f-89a9-180bfd8f75f8">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="06146160-2120-014b-4000-0ea3d9d6666e" Name="ID" Type="Guid Not Null" ReferencedColumn="5bfa9936-bb5a-008f-3100-080bfd8f75f8" />
	</SchemeComplexColumn>
	<SchemePhysicalColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="06146160-2120-004b-3100-0ea3d9d6666e" Name="RowID" Type="Guid Not Null" />
	<SchemeComplexColumn ID="36fbe24f-6866-41e1-84ab-29360d8002b4" Name="Responsible" Type="Reference(Typified) Not Null" ReferencedTable="6c977939-bbfc-456f-a133-f1c2244e3cc3">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="36fbe24f-6866-00e1-4000-09360d8002b4" Name="ResponsibleID" Type="Guid Not Null" ReferencedColumn="6c977939-bbfc-016f-4000-01c2244e3cc3" />
		<SchemeReferencingColumn ID="8c51f00c-fe9a-428c-b193-3ac36e253acb" Name="ResponsibleName" Type="String(128) Not Null" ReferencedColumn="1782f76a-4743-4aa4-920c-7edaee860964" />
	</SchemeComplexColumn>
	<SchemePhysicalColumn ID="f383f463-ecfa-4c0f-8d1c-f7d79ec0e56b" Name="Text" Type="String(Max) Not Null" />
	<SchemePhysicalColumn ID="c948ef1e-080c-4a74-98b8-7c5638c91413" Name="StateName" Type="String(Max) Not Null" />
	<SchemePrimaryKey IsSystem="true" IsPermanent="true" IsSealed="true" ID="06146160-2120-004b-5000-0ea3d9d6666e" Name="pk_DmTaskChildMessagesVirtual">
		<SchemeIndexedColumn Column="06146160-2120-004b-3100-0ea3d9d6666e" />
	</SchemePrimaryKey>
	<SchemeIndex IsSystem="true" IsPermanent="true" IsSealed="true" ID="06146160-2120-004b-7000-0ea3d9d6666e" Name="idx_DmTaskChildMessagesVirtual_ID" IsClustered="true">
		<SchemeIndexedColumn Column="06146160-2120-014b-4000-0ea3d9d6666e" />
	</SchemeIndex>
</SchemeTable>