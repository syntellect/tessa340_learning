﻿<?xml version="1.0" encoding="utf-8"?>
<SchemeTable Partition="b2812dfb-f8c6-4d27-babf-d34c45191add" ID="b33f3e71-fd32-4aeb-bf4f-1c6ddd0e8092" Name="RteLoanRequests" Group="Rte" InstanceType="Cards" ContentType="Entries">
	<Description>Заявка на кредит</Description>
	<SchemeComplexColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="b33f3e71-fd32-00eb-2000-0c6ddd0e8092" Name="ID" Type="Reference(Typified) Not Null" ReferencedTable="1074eadd-21d7-4925-98c8-40d1e5f0ca0e">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="b33f3e71-fd32-01eb-4000-0c6ddd0e8092" Name="ID" Type="Guid Not Null" ReferencedColumn="9a58123b-b2e9-4137-9c6c-5dab0ec02747" />
	</SchemeComplexColumn>
	<SchemePhysicalColumn ID="6e81ba07-35a2-46a9-a204-a2d1002d074c" Name="Number" Type="Int32 Not Null" IsIdentity="true" IdentityStart="0" IdentityIncrement="1">
		<Description>Номер заявки</Description>
	</SchemePhysicalColumn>
	<SchemePhysicalColumn ID="9a919078-55bf-42df-96af-f02e847fa99b" Name="ContractNumber" Type="String(100) Not Null">
		<Description>Номер договора</Description>
	</SchemePhysicalColumn>
	<SchemePhysicalColumn ID="c8d3faca-6809-44fd-b05e-9ef400d7ccb0" Name="Amount" Type="Double Not Null">
		<SchemeDefaultConstraint IsPermanent="true" ID="e9a3ba87-6cdb-4e64-a205-9806fe76f89a" Name="df_RteLoanRequests_Amount" Value="0" />
	</SchemePhysicalColumn>
	<SchemePhysicalColumn ID="90f006a1-90c7-41af-87ea-099ec5631fb8" Name="EndDate" Type="DateTime Not Null">
		<Description>Крайнее время платежа</Description>
	</SchemePhysicalColumn>
	<SchemePhysicalColumn ID="699b5458-714e-42e6-9307-aab8c3aa6997" Name="PaymentsCount" Type="Int16 Not Null">
		<Description>Количество этапов платежей</Description>
		<SchemeDefaultConstraint IsPermanent="true" ID="de664ad2-a562-446b-9bc8-f0e154c5f3da" Name="df_RteLoanRequests_PaymentsCount" Value="0" />
	</SchemePhysicalColumn>
	<SchemeComplexColumn ID="5bf42b22-a398-4635-846c-4d9255f1cf5c" Name="Client" Type="Reference(Typified) Not Null" ReferencedTable="eee9d301-64df-4c46-9105-4274dc2ad129">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="5bf42b22-a398-0035-4000-0d9255f1cf5c" Name="ClientID" Type="Guid Not Null" ReferencedColumn="eee9d301-64df-0146-4000-0274dc2ad129" />
		<SchemeReferencingColumn ID="e188d090-3e22-42c3-a819-04d26fd91404" Name="ClientFullName" Type="String(255) Not Null" ReferencedColumn="2a5b656a-e82e-4c13-a268-64b02ed1f44d" />
		<SchemeReferencingColumn ID="d1130956-5abc-4d29-ba12-a310f84dd771" Name="ClientBirthDate" Type="DateTime Not Null" ReferencedColumn="a7ebcdc4-c839-4ed0-8f3f-1e03e4e0375b" />
		<SchemeReferencingColumn ID="54684898-f0b3-40bc-ad8b-865fde44cb6f" Name="ClientRecidence" Type="String(255) Not Null" ReferencedColumn="1d96e7f0-bcf2-469d-aba7-631a564dc10e" />
	</SchemeComplexColumn>
	<SchemeComplexColumn ID="168fbfec-46c9-4447-8027-343a5e293b55" Name="State" Type="Reference(Typified) Null" ReferencedTable="47107d7a-3a8c-47f0-b800-2a45da222ff4">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="168fbfec-46c9-0047-4000-043a5e293b55" Name="StateID" Type="Int16 Null" ReferencedColumn="502209b0-233f-4e1f-be01-35a50f53414c" />
		<SchemeReferencingColumn ID="cdcfab92-c983-4b00-93a7-1da45f2f2fc6" Name="StateName" Type="String(128) Null" ReferencedColumn="4c1a8dd7-72ed-4fc9-b559-b38ae30dccb9" />
	</SchemeComplexColumn>
	<SchemePrimaryKey IsSystem="true" IsPermanent="true" IsSealed="true" ID="b33f3e71-fd32-00eb-5000-0c6ddd0e8092" Name="pk_RteLoanRequests" IsClustered="true">
		<SchemeIndexedColumn Column="b33f3e71-fd32-01eb-4000-0c6ddd0e8092" />
	</SchemePrimaryKey>
</SchemeTable>