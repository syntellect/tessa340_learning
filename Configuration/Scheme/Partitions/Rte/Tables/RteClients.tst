﻿<?xml version="1.0" encoding="utf-8"?>
<SchemeTable Partition="b2812dfb-f8c6-4d27-babf-d34c45191add" ID="eee9d301-64df-4c46-9105-4274dc2ad129" Name="RteClients" Group="Rte" InstanceType="Cards" ContentType="Entries">
	<Description>Клиенты</Description>
	<SchemeComplexColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="eee9d301-64df-0046-2000-0274dc2ad129" Name="ID" Type="Reference(Typified) Not Null" ReferencedTable="1074eadd-21d7-4925-98c8-40d1e5f0ca0e">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="eee9d301-64df-0146-4000-0274dc2ad129" Name="ID" Type="Guid Not Null" ReferencedColumn="9a58123b-b2e9-4137-9c6c-5dab0ec02747" />
	</SchemeComplexColumn>
	<SchemePhysicalColumn ID="2a5b656a-e82e-4c13-a268-64b02ed1f44d" Name="FullName" Type="String(255) Not Null">
		<Description>ФИО клиента</Description>
	</SchemePhysicalColumn>
	<SchemePhysicalColumn ID="a7ebcdc4-c839-4ed0-8f3f-1e03e4e0375b" Name="BirthDate" Type="DateTime Not Null">
		<Description>Дата рождения</Description>
	</SchemePhysicalColumn>
	<SchemePhysicalColumn ID="1d96e7f0-bcf2-469d-aba7-631a564dc10e" Name="Recidence" Type="String(255) Not Null">
		<Description>Город проживания</Description>
	</SchemePhysicalColumn>
	<SchemePrimaryKey IsSystem="true" IsPermanent="true" IsSealed="true" ID="eee9d301-64df-0046-5000-0274dc2ad129" Name="pk_RteClients" IsClustered="true">
		<SchemeIndexedColumn Column="eee9d301-64df-0146-4000-0274dc2ad129" />
	</SchemePrimaryKey>
</SchemeTable>