﻿<?xml version="1.0" encoding="utf-8"?>
<SchemeTable Partition="b2812dfb-f8c6-4d27-babf-d34c45191add" ID="f35ed754-d652-4a92-87aa-863c4111f078" Name="RteClientHistory" Group="Rte" InstanceType="Cards" ContentType="Collections">
	<Description>История кредитов клиента</Description>
	<SchemeComplexColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="f35ed754-d652-0092-2000-063c4111f078" Name="ID" Type="Reference(Typified) Not Null" ReferencedTable="1074eadd-21d7-4925-98c8-40d1e5f0ca0e">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="f35ed754-d652-0192-4000-063c4111f078" Name="ID" Type="Guid Not Null" ReferencedColumn="9a58123b-b2e9-4137-9c6c-5dab0ec02747" />
	</SchemeComplexColumn>
	<SchemePhysicalColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="f35ed754-d652-0092-3100-063c4111f078" Name="RowID" Type="Guid Not Null" />
	<SchemePhysicalColumn ID="4828e475-4508-4e93-bd41-123a057fe669" Name="ContractNumber" Type="String(Max) Not Null" />
	<SchemePhysicalColumn ID="699b2bad-aebc-4192-9f49-633ba1956360" Name="PlannedPaymentDate" Type="DateTime Not Null">
		<Description>Крайняя дата погашения по договору</Description>
	</SchemePhysicalColumn>
	<SchemePhysicalColumn ID="d4e6b2cb-2273-4a05-a44d-f53b69ef010d" Name="PaymentDate" Type="DateTime Null">
		<Description>Дата погашения кредита. Null - если ещё не погашен</Description>
	</SchemePhysicalColumn>
	<SchemePrimaryKey IsSystem="true" IsPermanent="true" IsSealed="true" ID="f35ed754-d652-0092-5000-063c4111f078" Name="pk_RteClientHistory">
		<SchemeIndexedColumn Column="f35ed754-d652-0092-3100-063c4111f078" />
	</SchemePrimaryKey>
	<SchemeIndex IsSystem="true" IsPermanent="true" IsSealed="true" ID="f35ed754-d652-0092-7000-063c4111f078" Name="idx_RteClientHistory_ID" IsClustered="true">
		<SchemeIndexedColumn Column="f35ed754-d652-0192-4000-063c4111f078" />
	</SchemeIndex>
</SchemeTable>