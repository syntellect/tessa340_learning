#tessa_exchange_format(Version:1, CreationTime:2018-09-25T11\:23\:17) {
	#exchange_view(RowID:775558fa-2ec3-4c38-819e-9395e94c28c7, Alias:Protocols, Caption:Протоколы, ModifiedById:3db19fa0-228a-497f-873a-0250bf0a4ccb, ModifiedByName:Admin, FormatVersion:2, ModifiedDateTime:2018-09-25T11\:22\:58, GroupName:KrDocuments) {
		#metadata {
			#view(DefaultSortColumn: CreationDate, DefaultSortDirection: desc, Paging: always, MultiSelect: true, QuickSearchParam: Number)

			#if(MSSQL) {
				#view(RowCountSubset: Count)
				#subset(Alias: Count)
			}

			#column(Alias: DocID, Hidden: true, Type: $DocumentCommonInfo.ID)
			#column(Alias: DocNumber, Caption: $Views_Registers_Number, Type: $DocumentCommonInfo.FullNumber, SortBy: mi.Number)
			#column(Alias: DocSubject, Caption: $Views_Registers_ProtocolSubject, Type: $DocumentCommonInfo.Subject, SortBy: mi.Subject)
			#column(Alias: DocDescription, Caption: $Views_Registers_DocDescription, Hidden: true, Type: String)
			#column(Alias: AuthorID, Hidden: true, Type: $DocumentCommonInfo.AuthorID)
			#column(Alias: AuthorName, Caption: $Views_Registers_Secretary, Type: $DocumentCommonInfo.AuthorName, SortBy: mi.AuthorName)
			#column(Alias: RegistratorID, Hidden: true, Type: $DocumentCommonInfo.RegistratorID)
			#column(Alias: RegistratorName, Caption: $Views_Registers_Registrator, Type: $DocumentCommonInfo.RegistratorName, SortBy: mi.RegistratorName)
			#column(Alias: ProtocolDate, Caption: $Views_Registers_ProtocolDate, Type: $Protocols.Date, SortBy: pr.Date)
			#column(Alias: CreationDate, Caption: $Views_Registers_CreationDate, Type: Date, SortBy: mi.CreationDate)
			#column(Alias: Department, Caption: $Views_Registers_Department, Type: $DocumentCommonInfo.DepartmentName)
			#reference(ColPrefix: Doc, RefSection: DocumentCommonInfo DocRefsSection, DisplayValueColumn: DocNumber, IsCard: true, OpenOnDoubleClick: true)
			#reference(ColPrefix: Author, RefSection: Roles DurableRoles PersonalRoles, DisplayValueColumn: AuthorName, IsCard: true, OpenOnDoubleClick: false)
			#reference(ColPrefix: Registrator, RefSection: Roles DurableRoles PersonalRoles, DisplayValueColumn: RegistratorName, IsCard: true, OpenOnDoubleClick: false)
			#param(Alias: IsAuthor, Caption: $Views_Registers_IsSecretary_Param, Type: Boolean, AllowedOperands: IsTrue IsFalse)
			#param(Alias: IsRegistrator, Caption: $Views_Registers_IsRegistrator_Param, Type: Boolean, AllowedOperands: IsTrue IsFalse)
			#param(Alias: Number, Caption: $Views_Registers_Number_Param, Multiple: true, Type: $DocumentCommonInfo.FullNumber)
			#param(Alias: Subject, Caption: $Views_Registers_ProtocolSubject_Param, Multiple: true, Type: $DocumentCommonInfo.Subject)
			#param(Alias: ProtocolDate, Caption: $Views_Registers_ProtocolDate_Param, Multiple: true, Type: $Protocols.Date)
			#param(Alias: Author, Caption: $Views_Registers_Secretary_Param, Multiple: true, Type: $DocumentCommonInfo.AuthorID, RefSection: PersonalRoles, AllowedOperands: Equality NonEquality) {
				#autocomplete(View: Users, Param: Name, PopupColumns: 1 4)
			}
			#param(Alias: Registrator, Caption: $Views_Registers_Registrator_Param, Multiple: true, Type: $DocumentCommonInfo.RegistratorID, RefSection: PersonalRoles, AllowedOperands: Equality NonEquality) {
				#autocomplete(View: Users, Param: Name, PopupColumns: 1 4)
			}
			#param(Alias: Department, Caption: $Views_Registers_Department_Param, Type: $DocumentCommonInfo.DepartmentID, RefSection: DepartmentRoles) {
				#autocomplete(View: Departments, Param: Name, PopupColumns: 1 3 5)
			}
		}
		#description {}
		#ms_query {
			\#var\(UseCommonInfo\: Normal || Number || Subject || IsAuthor || IsRegistrator || Author || Registrator || Department\)

			SELECT
				\#if\(Normal\) \{
				[t].[ID]																AS [DocID]\,
				[t].[FullNumber]														AS [DocNumber]\,
				[t].[Subject]															AS [DocSubject]\,
				COALESCE\([t].[FullNumber]\, N''\) + N'\, ' + COALESCE\([t].[Subject]\, N''\)	AS [DocDescription]\,		-- autocomplete
				[t].[AuthorID]															AS [AuthorID]\,
				[t].[AuthorName]														AS [AuthorName]\,
				[t].[RegistratorID]														AS [RegistratorID]\,
				[t].[RegistratorName]													AS [RegistratorName]\,
				[t].[Date]																AS [ProtocolDate]\,
				CAST\([t].[CreationDate] AS date\)										AS [CreationDate]\,
				[t].[DepartmentName]													AS [Department]
				\}
				\#if\(Count\) \{
				[t].*
				\}
			FROM \(
				SELECT
					\#if\(Normal\) \{
					[mi].[ID]\,
					[mi].[FullNumber]\,
					[mi].[Subject]\,
					[mi].[AuthorID]\,
					[mi].[AuthorName]\,
					[mi].[RegistratorID]\,
					[mi].[RegistratorName]\,
					[pr].[Date]\,
					[mi].[CreationDate]\,
					[mi].[DepartmentName]\,
					[t].[rn]
					\}
					\#if\(Count\) \{
					count\(*\) AS [cnt]
					\}
				FROM \(
					SELECT
						[mi].[ID]
						\#if\(Normal\) \{\,
						row_number\(\) OVER \(ORDER BY \#order_by\) AS [rn]
						\}
					\#if\(UseCommonInfo\) \{
					FROM [DocumentCommonInfo] AS [mi] WITH \(NOLOCK\)
					\} \{
					/* Алиас [mi] должен быть таким же\, как у DocumentCommonInfo */
					FROM [Instances] AS [mi] WITH \(NOLOCK\)
					\}
					\#if\(ProtocolDate || request.SortedBy\("ProtocolDate"\)\) \{
					INNER JOIN [Protocols] AS [pr] WITH \(NOLOCK\)
						ON [pr].[ID] = [mi].[ID]
					\}
					WHERE
						\#if\(UseCommonInfo\) \{
						[mi].[CardTypeID]
						\} \{
						[mi].[TypeID]
						\}
						= '4d9f9590-0131-4d32-9710-5e07c282b5d3'		-- Protocol
						\#param\(Number\, [mi].[FullNumber]\)
						\#param\(Subject\, [mi].[Subject]\)
						\#param\(ProtocolDate\, [pr].[Date]\)
						\#param\(Author\, [mi].[AuthorID]\)
						\#param\(Registrator\, [mi].[RegistratorID]\)
						\#param\(Department\, [mi].[DepartmentID]\)
						\#if\(IsAuthor\) \{
							\#if\(IsAuthor.CriteriaName == "IsTrue"\) \{
						AND [mi].[AuthorID] = \#param\(CurrentUserID\)
							\} \{
						AND \([mi].[AuthorID] IS NULL OR [mi].[AuthorID] <> \#param\(CurrentUserID\)\)
							\}
						\}
						\#if\(IsRegistrator\) \{
							\#if\(IsRegistrator.CriteriaName == "IsTrue"\) \{
						AND [mi].[RegistratorID] = \#param\(CurrentUserID\)
							\} \{
						AND \([mi].[RegistratorID] IS NULL OR [mi].[RegistratorID] <> \#param\(CurrentUserID\)\)
							\}
						\}
					\) AS [t]
				\#if\(Normal\) \{
				INNER JOIN [DocumentCommonInfo] AS [mi] WITH \(NOLOCK\)
					ON [mi].[ID] = [t].[ID]
				INNER JOIN [Protocols] AS [pr] WITH \(NOLOCK\)
					ON [pr].[ID] = [t].[ID]
				\}
				\#if\(PageOffset\) \{
				WHERE [t].[rn] >= \#param\(PageOffset\) AND [t].[rn] < \(\#param\(PageOffset\) + \#param\(PageLimit\)\)
				\}
				\) AS [t]
			\#if\(Normal\) \{
			LEFT JOIN [KrApprovalCommonInfo] AS [kr] WITH \(NOLOCK\)
				ON [kr].[MainCardID] = [t].[ID]
			ORDER BY [t].[rn]
			\}
			;
		}
		#pg_query {
			\#var\(UseCommonInfo\: Normal || Number || Subject || IsAuthor || IsRegistrator || Author || Registrator || Department\)

			SELECT
				\#if\(Normal\) \{
				"mi"."ID"																AS "DocID"\,
				"mi"."FullNumber"														AS "DocNumber"\,
				"mi"."Subject"															AS "DocSubject"\,
				COALESCE\("mi"."FullNumber"\, ''\) || '\, ' || COALESCE\("mi"."Subject"\, ''\)	AS "DocDescription"\,		-- autocomplete
				"mi"."AuthorID"															AS "AuthorID"\,
				"mi"."AuthorName"														AS "AuthorName"\,
				"mi"."RegistratorID"													AS "RegistratorID"\,
				"mi"."RegistratorName"													AS "RegistratorName"\,
				"pr"."Date"																AS "ProtocolDate"\,
				CAST\("mi"."CreationDate" AS date\)										AS "CreationDate"\,
				"mi"."DepartmentName"													AS "Department"
				\}
			FROM \(
				SELECT
					"mi"."ID"
				\#if\(UseCommonInfo\) \{
				FROM "DocumentCommonInfo" AS "mi"
				\} \{
				/* Алиас "mi" должен быть таким же\, как у DocumentCommonInfo */
				FROM "Instances" AS "mi"
				\}
				\#if\(ProtocolDate || request.SortedBy\("ProtocolDate"\)\) \{
				INNER JOIN "Protocols" AS "pr"
					ON "pr"."ID" = "mi"."ID"
				\}
				WHERE
					\#if\(UseCommonInfo\) \{
					"mi"."CardTypeID"
					\} \{
					"mi"."TypeID"
					\}
					= '4d9f9590-0131-4d32-9710-5e07c282b5d3'		-- Protocol
					\#param\(Number\, "mi"."FullNumber"\)
					\#param\(Subject\, "mi"."Subject"\)
					\#param\(ProtocolDate\, "pr"."Date"\)
					\#param\(Author\, "mi"."AuthorID"\)
					\#param\(Registrator\, "mi"."RegistratorID"\)
					\#param\(Department\, "mi"."DepartmentID"\)
					\#if\(IsAuthor\) \{
						\#if\(IsAuthor.CriteriaName == "IsTrue"\) \{
					AND "mi"."AuthorID" = \#param\(CurrentUserID\)
						\} \{
					AND \("mi"."AuthorID" IS NULL OR "mi"."AuthorID" <> \#param\(CurrentUserID\)\)
						\}
					\}
					\#if\(IsRegistrator\) \{
						\#if\(IsRegistrator.CriteriaName == "IsTrue"\) \{
					AND "mi"."RegistratorID" = \#param\(CurrentUserID\)
						\} \{
					AND \("mi"."RegistratorID" IS NULL OR "mi"."RegistratorID" <> \#param\(CurrentUserID\)\)
						\}
					\}
				\#if\(Normal\) \{
				ORDER BY \#order_by
				\}
				\#if\(PageOffset\) \{
				OFFSET \#param\(PageOffset\) - 1 LIMIT \#param\(PageLimit\)
				\}
				\) AS "t"
			\#if\(Normal\) \{
			INNER JOIN "DocumentCommonInfo" AS "mi"
				ON "mi"."ID" = "t"."ID"
			INNER JOIN "Protocols" AS "pr"
				ON "pr"."ID" = "t"."ID"
			LEFT JOIN "KrApprovalCommonInfo" AS "kr"
				ON "kr"."MainCardID" = "t"."ID"
			ORDER BY \#order_by
			\}
			;
		}
		#role(RoleID:0071b103-0ffa-49da-8776-53b9c654d815, ViewID:775558fa-2ec3-4c38-819e-9395e94c28c7) 
	}
}