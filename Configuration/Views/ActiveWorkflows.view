#tessa_exchange_format(Version:1, CreationTime:2018-09-05T21\:54\:57) {
	#exchange_view(RowID:68def22d-ade6-439f-bbc4-21ea18a3c409, Alias:ActiveWorkflows, Caption:Активные бизнес-процессы, ModifiedById:3db19fa0-228a-497f-873a-0250bf0a4ccb, ModifiedByName:Admin, FormatVersion:2, ModifiedDateTime:2018-09-05T21\:54\:23, GroupName:WorkflowEngine) {
		#metadata {
			#view(DefaultSortColumn: ProcessCreated, DefaultSortDirection: desc, Paging: optional, MultiSelect: true)

			#column(Alias: ProcessID, Hidden: true, Type: $WorkflowEngineProcesses.ID)
			#column(Alias: ProcessRowID, Hidden: true, Type: $WorkflowEngineProcesses.RowID)
			#column(Alias: ProcessName, Caption: $Views_ActiveWorkflows_ProcessName, Localizable: true, SortBy: pr.Name, Type: $WorkflowEngineProcesses.Name)
			#column(Alias: ProcessCreated, Caption: $Views_ActiveWorkflows_Created, SortBy: pr.Created, Type: $WorkflowEngineProcesses.Created)
			#column(Alias: ProcessLastActivity, Caption: $Views_ActiveWorkflows_LastActivity, SortBy: pr.LastActivity, Type: $WorkflowEngineProcesses.LastActivity)
			#column(Alias: ProcessCardID, Hidden: true, Type: $Instances.TypeID)
			#column(Alias: ProcessCardDigest, Caption: $Views_ActiveWorkflows_CardDigest, Localizable: true, SortBy: pr.CardDigest, Type: $WorkflowEngineProcesses.CardDigest)
			#column(Alias: ProcessCardType, Caption: $Views_ActiveWorkflows_CardType, Localizable: true, SortBy: LCaption.Value, Type: $Instances.TypeCaption)

			#param(Alias: ProcessTemplate, Caption: $Views_ActiveWorkflows_ProcessTemplate_Param, Type: $BusinessProcessVersions.ID, RefSection: BusinessProcessInfo, Multiple: true){
				#autocomplete(View: BusinessProcesses, Param: Name)
			}
			#param(Alias: CardType, Caption: $Views_ActiveWorkflows_CardType_Param, Type: $Instances.TypeID, RefSection: Types, Multiple: true){
				#autocomplete(View: Types, Param: Caption)
			}
			#param(Alias: CardDigest, Caption: $Views_ActiveWorkflows_CardDigest_Param, Type: $WorkflowEngineProcesses.CardDigest, Multiple: true)
			#param(Alias: Created, Caption: $Views_ActiveWorkflows_Created_Param, Type: $WorkflowEngineProcesses.Created, Multiple: true)
			#param(Alias: LastActivity, Caption: $Views_ActiveWorkflows_LastActivity_Param, Type: $WorkflowEngineProcesses.LastActivity, Multiple: true)
			#param(Alias: CardID, Caption: $Views_ActiveWorkflows_CardID_Param, Type: $WorkflowEngineProcesses.CardID, Hidden: true)
			#param(Alias: HasActiveErrors, Caption: $Views_ActiveWorkflows_HasActiveErrors_Param, Type: Boolean, Hidden: true, AllowedOperands: IsTrue)
			#param(Alias: ProcessInstance, Caption: Process instance, Type: $WorkflowEngineProcesses.ID, Hidden: true)

			#reference(ColPrefix: Process, RefSection: BusinessProcessInfo, DisplayValueColumn: ProcessName, IsCard: true, OpenOnDoubleClick: true)
		}
		#description {}
		#ms_query {
			DECLARE @NoName nvarchar\(32\) = [dbo].[GetString]\(N'Views_ActiveWorkflows_NoNameStr_Sql'\, \#param\(locale\)\);

			SELECT
				[t].[ProcessID]
				\,[t].[ProcessRowID]
				\,[t].[ProcessName]
				\,[t].[ProcessCreated]
				\,[t].[ProcessLastActivity]
				\,[t].[ProcessCardID]
				\,[t].[ProcessCardDigest]
				\,[t].[ProcessCardType]
			FROM
			\(
				SELECT		
					[pr].[ID] as [ProcessID]
					\,[pr].[RowID] as [ProcessRowID]
					\,[pr].[Name] as [ProcessName]
					\,[pr].[Created] as [ProcessCreated]
					\,[pr].[LastActivity] as [ProcessLastActivity]
					\,[pr].[CardID] as [ProcessCardID]
					\,ISNULL\([pr].[CardDigest]\, @NoName\) as [ProcessCardDigest]
					\,[t].[Caption] as [ProcessCardType]
					\,row_number\(\) over \(order by \#order_by\) as [rn]
					
				FROM [WorkflowEngineProcesses] [pr] WITH\(NOLOCK\)
				INNER JOIN [Instances] [i] WITH\(NOLOCK\) on [pr].[CardID] = [i].[ID]
				INNER JOIN [Types] [t] WITH\(NOLOCK\) on [i].[TypeID] = [t].[ID]
				\#if\(ProcessTemplate\)\{
				INNER JOIN [BusinessProcessVersions] [bpv] WITH\(NOLOCK\) ON [bpv].[RowID] = [pr].[ProcessTemplateRowID]
				\}
				\#if\(Normal && request.SortedBy\("ProcessCardType"\)\) \{
					CROSS APPLY [Localization]\([t].[Caption]\, \#param\(locale\)\) AS [LCaption]
				\}
				\#if\(HasActiveErrors\)\{
				CROSS APPLY
				\(
					SELECT TOP 1 1 as [tmp]
					FROM [WorkflowEngineNodes] [wen] WITH\(NOLOCK\)
					JOIN [WorkflowEngineErrors] [wee] WITH\(NOLOCK\) ON [wen].[RowID] = [wee].[NodeRowID] 
					WHERE  [wen].[ProcessRowID] = [pr].[RowID] AND [wee].[Active] = 1 
				\) [c]
				\}
				WHERE 1 = 1
					\#param\(CardID\, [pr].[CardID]\)
					\#param\(CardDigest\, [pr].[CardDigest]\)
					\#param\(CardType\, [i].[TypeID]\)
					\#param\(ProcessTemplate\, [bpv].[ID]\)
					\#param\(ProcessInstance\, [pr].[ID]\)
			\) [t]
			\#if\(PageOffset\) \{
			    where [t].[rn] >= \#param_expr\(PageOffset\) and [t].[rn] < \(\#param_expr\(PageOffset\) + \#param_expr\(PageLimit\)\)
			\}
			 order by [t].[rn]
		}
		#pg_query {
			DECLARE "no_name" text;

			BEGIN
				"no_name" = "GetString"\('Views_ActiveWorkflows_NoNameStr_Sql'\, \#param\(locale\)\);
				
				RETURN QUERY
				SELECT
					"t"."ProcessID"
					\,"t"."ProcessRowID"
					\,"t"."ProcessName"
					\,"t"."ProcessCreated"
					\,"t"."ProcessLastActivity"
					\,"t"."ProcessCardID"
					\,"t"."ProcessCardDigest"
					\,"t"."ProcessCardType"
				FROM
				\(
					SELECT
						"pr"."ID" as "ProcessID"
						\,"pr"."RowID" as "ProcessRowID"
						\,"pr"."Name" as "ProcessName"
						\,"pr"."Created" as "ProcessCreated"
						\,"pr"."LastActivity" as "ProcessLastActivity"
						\,"pr"."CardID" as "ProcessCardID"
						\,coalesce\("pr"."CardDigest"\, "no_name"\) as "ProcessCardDigest"
						\,"t"."Caption" as "ProcessCardType"
						\,row_number\(\) over \(order by \#order_by\) as "rn"
						
					FROM "WorkflowEngineProcesses" "pr"
					INNER JOIN "Instances" "i" on "pr"."CardID" = "i"."ID"
					INNER JOIN "Types" "t" on "i"."TypeID" = "t"."ID"
					\#if\(ProcessTemplate\)\{
					INNER JOIN "BusinessProcessVersions" "bpv" ON "bpv"."RowID" = "pr"."ProcessTemplateRowID"
					\}
					\#if\(Normal && request.SortedBy\("ProcessCardType"\)\) \{
					CROSS JOIN "Localization"\("t"."Caption"\, \#param\(locale\)\) AS "LCaption"
					\}
					\#if\(HasActiveErrors\)\{
					INNER JOIN LATERAL
					\(
						SELECT null as "tmp"
						FROM "WorkflowEngineNodes" "wen"
						JOIN "WorkflowEngineErrors" "wee" ON "wen"."RowID" = "wee"."NodeRowID" 
						WHERE  "wen"."ProcessRowID" = "pr"."RowID" AND "wee"."Active"
						LIMIT 1
					\) "c" ON true
					\}
					WHERE true
						\#param\(CardID\, "pr"."CardID"\)
						\#param\(CardDigest\, "pr"."CardDigest"\)
						\#param\(CardType\, "i"."TypeID"\)
						\#param\(ProcessTemplate\, "bpv"."ID"\)
						\#param\(ProcessInstance\, "pr"."ID"\)
				\) "t"
				\#if\(PageOffset\) \{
					where "t"."rn" >= \#param_expr\(PageOffset\) and "t"."rn" < \(\#param_expr\(PageOffset\) + \#param_expr\(PageLimit\)\)
				\}
				order by "t"."rn";
				 
			END;
		}
		#role(RoleID:7ff52dc0-ff6a-4c9d-ba25-b562c370004d, ViewID:68def22d-ade6-439f-bbc4-21ea18a3c409) 
	}
}