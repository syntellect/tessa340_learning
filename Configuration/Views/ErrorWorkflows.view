#tessa_exchange_format(Version:1, CreationTime:2018-09-05T21\:54\:57) {
	#exchange_view(RowID:91bae5ac-e846-4b71-a87b-3cee38381c66, Alias:ErrorWorkflows, Caption:Бизнес-процессы с ошибками, ModifiedById:3db19fa0-228a-497f-873a-0250bf0a4ccb, ModifiedByName:Admin, FormatVersion:2, ModifiedDateTime:2018-09-05T21\:54\:23, GroupName:WorkflowEngine) {
		#metadata {
			#view(DefaultSortColumn: ProcessErrorAdded, DefaultSortDirection: desc, Paging: optional, MultiSelect: true)

			#column(Alias: ProcessID, Hidden: true, Type: $WorkflowEngineProcesses.ID)
			#column(Alias: ProcessRowID, Hidden: true, Type: $WorkflowEngineProcesses.RowID)
			#column(Alias: ProcessName, Caption: $Views_ErrorWorkflows_ProcessName, Localizable: true, SortBy: pr.Name, Type: $WorkflowEngineProcesses.Name)
			#column(Alias: ProcessCreated, Caption: $Views_ErrorWorkflows_Created, SortBy: pr.Created, Type: $WorkflowEngineProcesses.Created)
			#column(Alias: ProcessErrorAdded, Caption: $Views_ErrorWorkflows_LastErrorAdded, SortBy: e.ErrorAdded, Type: $WorkflowEngineErrors.Added)
			#column(Alias: ProcessCardID, Hidden: true, Type: $Instances.TypeID)
			#column(Alias: ProcessCardDigest, Caption: $Views_ErrorWorkflows_CardDigest, Localizable: true, SortBy: pr.CardDigest, Type: $WorkflowEngineProcesses.CardDigest)
			#column(Alias: ProcessCardType, Caption: $Views_ErrorWorkflows_CardType, Localizable: true, SortBy: LCaption.Value, Type: $Instances.TypeCaption)
			#column(Alias: ProcessResumable, Caption: $Views_ErrorWorkflows_Resumable, Localizable: true, SortBy: e.Resumable, Type: $WorkflowEngineErrors.Resumable)

			#param(Alias: ProcessTemplate, Caption: $Views_ErrorWorkflows_ProcessTemplate_Param, Type: $BusinessProcessVersions.ID, RefSection: BusinessProcessInfo, Multiple: true){
				#autocomplete(View: BusinessProcesses, Param: Name)
			}
			#param(Alias: CardType, Caption: $Views_ErrorWorkflows_CardType_Param, Type: $Instances.TypeID, RefSection: Types, Multiple: true){
				#autocomplete(View: Types, Param: Caption)
			}
			#param(Alias: CardDigest, Caption: $Views_ErrorWorkflows_CardDigest, Type: $WorkflowEngineProcesses.CardDigest, Multiple: true)
			#param(Alias: CardID, Caption: CardID, Type: $WorkflowEngineProcesses.CardID, Hidden: true)
			#param(Alias: ErrorText, Caption: $Views_ErrorWorkflows_ErrorText_Param, Type: $Errors.Text, Multiple: true)
			#param(Alias: ResumableOnly, Caption: $Views_ErrorWorkflows_ResumableOnly_Param, Type: $WorkflowEngineErrors.Resumable, AllowedOperands: IsTrue)

			#reference(ColPrefix: Process, RefSection: BusinessProcessInfo, DisplayValueColumn: ProcessName, IsCard: true, OpenOnDoubleClick: true)
		}
		#description {}
		#ms_query {
			DECLARE @NoName nvarchar\(32\) = [dbo].[GetString]\(N'Views_ErrorWorkflows_NoNameStr_Sql'\, \#param\(locale\)\);
			DECLARE @false bit = 0;
			DECLARE @true bit = 1;

			SELECT
				[t].[ProcessID]
				\,[t].[ProcessRowID]
				\,[t].[ProcessName]
				\,[t].[ProcessCreated]
				\,[t].[ProcessErrorAdded]
				\,[t].[ProcessCardID]
				\,[t].[ProcessCardDigest]
				\,[t].[ProcessCardType]
				\,[t].[ProcessResumable]
			FROM
			\(
				SELECT
					[pr].[ID] as [ProcessID]
					\,[pr].[RowID] as [ProcessRowID]
					\,[pr].[Name] as [ProcessName]
					\,[pr].[Created] as [ProcessCreated]
					\,[e].[ErrorAdded] as [ProcessErrorAdded]
					\,[pr].[CardID] as [ProcessCardID]
					\,coalesce\([pr].[CardDigest]\, @NoName\) as [ProcessCardDigest]
					\,[t].[Caption] as [ProcessCardType]
					\,[e].[Resumable] as [ProcessResumable]
					\,row_number\(\) over \(order by \#order_by\) as [rn]
					
				FROM [WorkflowEngineProcesses] [pr] WITH\(NOLOCK\)
				INNER JOIN [Instances] [i] WITH\(NOLOCK\) on [pr].[CardID] = [i].[ID]
				INNER JOIN [Types] [t] WITH\(NOLOCK\) on [i].[TypeID] = [t].[ID]
				\#if\(ProcessTemplate\)\{
				INNER JOIN [BusinessProcessVersions] [bpv] WITH\(NOLOCK\) ON [bpv].[RowID] = [pr].[ProcessTemplateRowID]
				\}
				\#if\(Normal && request.SortedBy\("ProcessCardType"\)\) \{
					CROSS APPLY [Localization]\([t].[Caption]\, \#param\(locale\)\) AS [LCaption]
				\}
				CROSS APPLY
				\(
					SELECT TOP 1 
						\#if\(ResumableOnly\)\{
							@true as [Resumable]
						\}\{
							coalesce\([res].[Resumable]\, @false\) as [Resumable]
						\}
						\,[wee].[Added] as [ErrorAdded]
					FROM [WorkflowEngineNodes] [wen] WITH\(NOLOCK\)
					INNER JOIN [WorkflowEngineErrors] [wee] WITH\(NOLOCK\) ON [wen].[RowID] = [wee].[NodeRowID] 
					\#if\(ErrorText\)\{
					INNER JOIN [Errors] [e] WITH\(NOLOCK\) ON [e].[ID] = [wee].[ErrorCardID]
					\}
					\#if\(!ResumableOnly\)
					\{
					OUTER APPLY
					\(
						SELECT TOP 1 [ee].[Resumable] as [Resumable]
						FROM [WorkflowEngineNodes] [nn] WITH\(NOLOCK\) 
						JOIN [WorkflowEngineErrors] [ee] WITH\(NOLOCK\) ON [nn].[RowID] = [ee].[NodeRowID]
						WHERE [nn].[ProcessRowID] = [pr].[RowID] AND [ee].[Resumable] = 1
					\) [res]
					\}
					WHERE  [wen].[ProcessRowID] = [pr].[RowID] 
						AND [wee].[Active] = 1
						\#param\(ResumableOnly\, [wee].[Resumable]\)
						\#param\(ErrorText\, [e].[Text]\)
					ORDER BY [wee].[Added] desc
				\) e
				WHERE 1 = 1
					\#param\(CardID\, [pr].[CardID]\)
					\#param\(CardDigest\, [pr].[CardDigest]\)
					\#param\(CardType\, [i].[TypeID]\)
					\#param\(ProcessTemplate\, [bpv].[ID]\)
			\) t
			\#if\(PageOffset\) \{
			    where [t].[rn] >= \#param_expr\(PageOffset\) and [t].[rn] < \(\#param_expr\(PageOffset\) + \#param_expr\(PageLimit\)\)
			\}
			 order by [t].[rn]
		}
		#pg_query {
			DECLARE "no_name" text;

			BEGIN
				"no_name" = "GetString"\('Views_ErrorWorkflows_NoNameStr_Sql'\, \#param\(locale\)\);
				
				RETURN QUERY
				SELECT
					"t"."ProcessID"
					\,"t"."ProcessRowID"
					\,"t"."ProcessName"
					\,"t"."ProcessCreated"
					\,"t"."ProcessErrorAdded"
					\,"t"."ProcessCardID"
					\,"t"."ProcessCardDigest"
					\,"t"."ProcessCardType"
					\,"t"."ProcessResumable"
				FROM
				\(
					SELECT
						"pr"."ID" as "ProcessID"
						\,"pr"."RowID" as "ProcessRowID"
						\,"pr"."Name" as "ProcessName"
						\,"pr"."Created" as "ProcessCreated"
						\,"e"."ErrorAdded" as "ProcessErrorAdded"
						\,"pr"."CardID" as "ProcessCardID"
						\,coalesce\("pr"."CardDigest"\, "no_name"\) as "ProcessCardDigest"
						\,"t"."Caption" as "ProcessCardType"
						\,"e"."Resumable" as "ProcessResumable"
						\,row_number\(\) over \(order by \#order_by\) as "rn"
						
					FROM "WorkflowEngineProcesses" "pr"
					INNER JOIN "Instances" "i" on "pr"."CardID" = "i"."ID"
					INNER JOIN "Types" "t" on "i"."TypeID" = "t"."ID"
					\#if\(ProcessTemplate\)\{
					INNER JOIN "BusinessProcessVersions" "bpv" ON "bpv"."RowID" = "pr"."ProcessTemplateRowID"
					\}
					\#if\(Normal && request.SortedBy\("ProcessCardType"\)\) \{
					CROSS JOIN "Localization"\("t"."Caption"\, \#param\(locale\)\) AS "LCaption"
					\}
					INNER JOIN LATERAL
					\(
						SELECT
							\#if\(ResumableOnly\)\{
								true as "Resumable"
							\}\{
								coalesce\("res"."Resumable"\, false\) as "Resumable"
							\}
							\,"wee"."Added" as "ErrorAdded"
						FROM "WorkflowEngineNodes" "wen"
						INNER JOIN "WorkflowEngineErrors" "wee" ON "wen"."RowID" = "wee"."NodeRowID" 
						\#if\(ErrorText\)\{
						INNER JOIN "Errors" "e" ON "e"."ID" = "wee"."ErrorCardID"
						\}
						\#if\(!ResumableOnly\)
						\{
						LEFT JOIN LATERAL
						\(
							SELECT true as "Resumable"
							FROM "WorkflowEngineNodes" "nn"  
							INNER JOIN "WorkflowEngineErrors" "ee" ON "nn"."RowID" = "ee"."NodeRowID"
							WHERE "nn"."ProcessRowID" = "pr"."RowID" AND "ee"."Resumable"
							LIMIT 1
						\) "res" on true
						\}
						WHERE  "wen"."ProcessRowID" = "pr"."RowID" 
							AND "wee"."Active"
							\#param\(ResumableOnly\, "wee"."Resumable"\)
							\#param\(ErrorText\, "e"."Text"\)			
						ORDER BY "wee"."Added" desc
						LIMIT 1
					\) e on true
					WHERE true
						\#param\(CardID\, "pr"."CardID"\)
						\#param\(CardDigest\, "pr"."CardDigest"\)
						\#param\(CardType\, "i"."TypeID"\)
						\#param\(ProcessTemplate\, "bpv"."ID"\)
				\) "t"
				\#if\(PageOffset\) \{
					where "t"."rn" >= \#param_expr\(PageOffset\) and "t"."rn" < \(\#param_expr\(PageOffset\) + \#param_expr\(PageLimit\)\)
				\}
				order by "t"."rn";
				 
			END;
		}
		#role(RoleID:7ff52dc0-ff6a-4c9d-ba25-b562c370004d, ViewID:91bae5ac-e846-4b71-a87b-3cee38381c66) 
	}
}